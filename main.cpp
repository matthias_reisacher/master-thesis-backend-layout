#include <string>
#include <iostream>

#include <grpcpp/grpcpp.h>
#include <grpc/support/log.h>

#include "dago-layout/layout_file.grpc.pb.h"
#include "dago-layout/layout_file.pb.h"

#include "processor.h"

const std::string GRPC_HOST("0.0.0.0");

class ProcessFileServiceImpl final : public ::dago_web::ProcessFileService::Service {
    ::grpc::Status process(::grpc::ServerContext *context, const ::dago_web::LayoutFileRequest *request,
                           ::dago_web::LayoutFileResponse *response) override {

        std::cout << "Processing file with id " << request->id() << std::endl;

        Processor *processor = new Processor();
        bool res = processor->processFile(request->id());

        response->set_success(res);

        return ::grpc::Status::OK;
    }
};

void run_server(std::string host, std::string port) {
    std::string server_address(host + ":" + port);
    ProcessFileServiceImpl service;

    grpc::ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);
    std::unique_ptr <grpc::Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
}

int main(int argc, char *argv[]) {
    const std::string grpc_port = std::getenv("GRPC_PORT");

    run_server(GRPC_HOST, grpc_port);

    return 0;
}