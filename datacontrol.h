#ifndef DATACONTROL_H
#define DATACONTROL_H

#include <QString>
#include <QDir>
#include <QMap>
#include <QTextStream>

#include "node-edge.h"

struct MinMax {
    MinMax(float min, float max) {
        this->min = min;
        this->max = max;
    }

    double min, max;
};


class DataControl {
public:
    DataControl();

    Graph *getGraph();

    void loadFileFromString(QString file);

    void loadFilesFromLocation(QString folder);

    bool readMore(int numlines);

    bool readNextFile();

    void writeToFile(string filename);

    std::string graphToCSV();

private:
    bool processFile(QTextStream *file);

    void processDataIn();

    void processLine();

    void addStringMapping(string property, string value);

    void setStringMapping(string property, string value, int index);

    void setAllStringMappings();

    vector <string> getStringValues(string property);

    void isMinMax(QString name, double value);

    map<string, int> stringmappings;
    map<string, int> stringmappingindex;

    Graph *graph;

    QString folder;

    vector<int> header;
    QList <QString> nodelabel;
    QList <QString> edgelabel;
    QList <QString> graphlabel;
    QStringList graphnames;

    vector<MinMax *> minmax;
    QMap<QString, int> minmaxmap;

    vector<int> edgeproperty, nodeproperty;

    QFile *datainput;
    QTextStream *datain;

    int indextype;
    int fromid, toid;
};

#endif // DATACONTROL_H
