#include "processor.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <QString>
#include <string>

#include "layout/GM3Interface.h"
#include "node-edge.h"
#include "datacontrol.h"

Processor::Processor() {
    const std::string redis_host = std::getenv("REDIS_HOST");
    const int redis_port = std::stoi(std::getenv("REDIS_PORT"));

    this->redis = new RedisService(redis_host, redis_port);
}

bool Processor::processFile(std::string id) {
    std::string inputFile = redis->readInputFile(id);

    if (inputFile.empty()) {
        std::cerr << "Error: Could not read file with id " << id << " from redis!" << std::endl;
        return false;
    }

//    std::cout << "File:\n" << inputFile << std::endl;

    //holds graph and loader
    DataControl *data = new DataControl();
    data->loadFileFromString(QString::fromStdString(inputFile));

    //initial calculation step
    std::cout << "Starting with layout computation ..." << std::endl;
    computeLayout(data->getGraph());
    std::cout << "... layout computation finished." << std::endl;

    //Determines which nodes need to read just and moves them
    std::cout << "Starting with refinement steps ..." << std::endl;
    const int num_iterations = 3;
    for(int i = 0 ; i < num_iterations; i++) {
        std::cout << "Refinement step " << i << " of " << num_iterations << std::endl;
        computeOneRefinementStep(data->getGraph());
    }
    std::cout << "... refinement steps finished." << std::endl;

    std::string csv = data->graphToCSV();
//    std::cout << std::endl << std::endl << "RESULT:\n" << csv << std::endl;

    std::cout << "Storing output file in cache." << std::endl;
    redis->writeOutputFile(id, csv);
    redis->deleteInputFile(id);

    std::cout << "Successfully processed file with id " << id << std::endl;
    return true;
}