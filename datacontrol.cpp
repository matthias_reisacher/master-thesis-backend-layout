#include "datacontrol.h"
#include <QTextStream>
#include <fstream>
#include <sstream>

DataControl::DataControl() {
    this->graph = new Graph();
    this->datain = 0;
    this->datainput = 0;
}

void DataControl::addStringMapping(string property, string value) {
    string hash = property + "." + value;
    if (stringmappings.find(hash) != stringmappings.end()) {
        return;
    }
    if (stringmappingindex.find(property) == stringmappingindex.end()) {
        stringmappingindex[property] = 0;
    }
    int index = stringmappingindex[property];
    stringmappings[hash] = index;
    stringmappingindex[property] = index + 1;
}

void DataControl::setStringMapping(string property, string value, int index) {
    string hash = property + "." + value;
    stringmappings[hash] = index;
}


vector <string> DataControl::getStringValues(string property) {
    list <string> temp;

    for (map<string, int>::const_iterator iter = stringmappings.begin(); iter != stringmappings.end(); ++iter) {
        string s = (iter->first);
        int idx = s.find(property + ".");
        if (idx == 0) {
            temp.push_back(s.substr(idx + property.length() + 1));
        }
    }

    temp.sort();
    vector <string> vec(temp.begin(), temp.end());
    return vec;
}


void DataControl::isMinMax(QString name, double value) {
    if (minmaxmap.find(name) == minmaxmap.end()) {
        minmaxmap[name] = minmax.size();
        minmax.push_back(new MinMax(value, value));
    }
    minmax[minmaxmap[name]]->max = minmax[minmaxmap[name]]->max < value ? value : minmax[minmaxmap[name]]->max;
    minmax[minmaxmap[name]]->min = minmax[minmaxmap[name]]->min > value ? value : minmax[minmaxmap[name]]->min;
}


void DataControl::processLine() {
    QString line;
    QStringList segment;

    line = this->datain->readLine();
    segment = line.split("\t");

    if (segment[this->indextype] == "Node") {
        Node *node = new Node;
        Property *p = 0;
        int nodeid = -1;

//        if (segment.size() != this->graphlabel.size())
//            printf("A line doesn't match the size of the label\n");

        for (int i = 0; i < segment.size(); i++) {
            if (!this->nodeproperty[i])
                continue;
            if (this->graphlabel[i] == "NodeId")
                nodeid = segment[i].toInt();
            switch (this->header[i]) {
                case PROPERTY_BOOL:
                    p = new BoolProperty(segment[i].toInt());
                    break;
                case PROPERTY_STRING:
                    p = new StringProperty(segment[i].toStdString());
                    addStringMapping(this->graphlabel[i].toStdString(), segment[i].toStdString());
                    break;
                case PROPERTY_INT:
                    p = new IntProperty(segment[i].toInt());
                    isMinMax(this->graphlabel[i], segment[i].toInt());
                    break;
                case PROPERTY_FLOAT:
                    p = new FloatProperty(segment[i].toFloat());
                    isMinMax(this->graphlabel[i], segment[i].toFloat());
                    break;
            }

            node->setProperty(this->graphlabel[i].toStdString(), p);

        }
        node->id = nodeid;
        if (this->graph->nodemap.find(node->id) == this->graph->nodemap.end()) {
            this->graph->nodemap[nodeid] = node;
            this->graph->nodes.push_back(node);
        }
    }//if Node
    else {// if Edge
        int from = segment[this->fromid].toInt();
        int to = segment[this->toid].toInt();
        if (this->graph->nodemap.find(from) == this->graph->nodemap.end() ||
            this->graph->nodemap.find(to) == this->graph->nodemap.end()) {
            printf("Missing the node information.\n");
            return;
        }

        Edge *edge = new Edge(this->graph->nodemap[from], this->graph->nodemap[to]);
        edge->setEdgeIds(from, to);

        GM3::edge *edgefound = this->graph->nodemap[from]->findEdge(to);


        if (!edgefound) {
            this->graph->edges.push_back(edge);
            this->graph->nodemap[from]->edges.push_back(edge);
        }


        Edge *edge2 = new Edge(this->graph->nodemap[to], this->graph->nodemap[from]);
        edge2->setEdgeIds(to, from);


        edgefound = this->graph->nodemap[to]->findEdge(from);
        if (!edgefound) {
            this->graph->edges.push_back(edge2);
            this->graph->nodemap[to]->edges.push_back(edge2);
        }


        Property *p = 0;

        for (int i = 0; i < segment.size(); i++) {
            if (!this->edgeproperty[i])
                continue;
            switch (this->header[i]) {
                case PROPERTY_BOOL:
                    p = new BoolProperty(segment[i].toInt());
                    break;
                case PROPERTY_STRING:
                    p = new StringProperty(segment[i].toStdString());
                    addStringMapping(this->graphlabel[i].toStdString(), segment[i].toStdString());
                    break;
                case PROPERTY_INT:
                    p = new IntProperty(segment[i].toInt());
                    isMinMax(this->graphlabel[i], segment[i].toInt());
                    break;
                case PROPERTY_FLOAT:
                    p = new FloatProperty(segment[i].toFloat());
                    isMinMax(this->graphlabel[i], segment[i].toFloat());
                    break;
            }
            edge->setProperty(this->graphlabel[i].toStdString(), p);
            edge2->setProperty(this->graphlabel[i].toStdString(), p);
        }
    }//if Edge

}


bool DataControl::readNextFile() {
    if (this->graphnames.size() == 0)
        return false;

    if (datainput) {
        datainput->close();
        datainput = 0;
    }

    QString graphfile = folder + "/" + this->graphnames[0];
    this->graphnames.pop_front();
    this->datainput = new QFile(graphfile);

    if (!this->datainput->open(QIODevice::ReadOnly | QIODevice::Text)) {
        printf("Could not load file '%s'\n", graphfile.toStdString().c_str());
        return false;
    }

    return processFile(new QTextStream(this->datainput));
}

bool DataControl::processFile(QTextStream *file) {
    datain = file;

    QString line = this->datain->readLine();
    QStringList segment = line.split("\t");

    this->nodeproperty.resize(segment.size(), 0);
    this->edgeproperty.resize(segment.size(), 0);

    for (int i = 0; i < segment.size(); i++) {
        if (segment[i] == "NODE") {
            this->nodeproperty[i] = 1;
        } else if (segment[i] == "EDGE") {
            this->edgeproperty[i] = 1;
        } else if (segment[i] == "BOTH") {
            this->nodeproperty[i] = 1;
            this->edgeproperty[i] = 1;
        }
    }

    line = this->datain->readLine();
    segment = line.split("\t");

    header.clear();
    for (int i = 0; i < segment.size(); i++) {
        if (segment[i] == "INT")
            this->header.push_back(PROPERTY_INT);
        else if (segment[i] == "BOOL")
            this->header.push_back(PROPERTY_BOOL);
        else if (segment[i] == "STRING")
            this->header.push_back(PROPERTY_STRING);
        else if (segment[i] == "FLOAT")
            this->header.push_back(PROPERTY_FLOAT);
    }


    line = this->datain->readLine();
    segment = line.split("\t");

    this->graphlabel.clear();
    for (int i = 0; i < segment.size(); i++) {
        if (this->nodeproperty[i])
            this->nodelabel.push_back(segment[i]);
        if (this->edgeproperty[i])
            this->edgelabel.push_back(segment[i]);

        this->graphlabel.push_back(segment[i]);
    }

    if (this->graphlabel.size() != (int) this->header.size()) {
        printf("Initial Header does not match \n");
        return false;
    }

    int indexoperation = segment.indexOf("Operation");
    if (indexoperation == -1) {
        printf("The variable 'Operation' is missing. Look up the README for variable description.");
        return false;
    }

    this->indextype = segment.indexOf("Type");
    if (this->indextype == -1) {
        printf("The variable 'Type' is missing. Look up the README for variable description.");
        return false;
    }

    this->fromid = segment.indexOf("From");
    this->toid = segment.indexOf("To");
    if (this->fromid == -1 || this->toid == -1) {
        printf("The variable 'From' or 'To' is missing. Look up the README for variable description.");
        return false;
    }

    printf("Node Label: %d Header: %lu \n", this->graphlabel.size(), this->header.size());
    return true;
}

void DataControl::setAllStringMappings() {
    for (unsigned int i = 0; i < this->header.size(); i++) {
        if (this->header[i] == PROPERTY_STRING) {
            vector <string> values = getStringValues(this->graphlabel[i].toStdString());
            for (unsigned int j = 0; j < values.size(); j++) {
                setStringMapping(this->graphlabel[i].toStdString(), values[j], j);
            }
        }
    }
}

void DataControl::loadFileFromString(QString file) {
    processFile(new QTextStream(&file));
    processDataIn();
}

void DataControl::loadFilesFromLocation(QString folder) {
    printf("Loading files from folder %s\n", folder.toUtf8().constData());
    this->folder = folder;

    QDir *qdir = new QDir(folder);
    qdir->setFilter(QDir::Files);
    qdir->setSorting(QDir::Name);
    QFileInfoList listgraph = qdir->entryInfoList();

    printf("Checking names of %d files:\n", listgraph.size());
    for (int i = 0; i < listgraph.size(); i++) {
        QFileInfo fileinfo = listgraph.at(i);
        printf("\t [%d]: %s\n", i, fileinfo.fileName().toUtf8().constData());
        if (fileinfo.fileName().contains("graph"))
            graphnames.push_back(fileinfo.fileName());
    }

    readNextFile();
    processDataIn();
}

void DataControl::processDataIn() {
    while (!datain->atEnd()) {
        processLine();
    }//while there is still to read

    setAllStringMappings();

    printf("Graph Load completed\n");
    printf("Saving %d node attributes and %d edge attributes.\n", nodelabel.size(), edgelabel.size());
    printf("This file contains %d Nodes and %d Edges.\n", graph->numNodes(), graph->numEdges());
}

bool DataControl::readMore(int numlines) {
    for (int i = 0; i < numlines || numlines == 0; i++) {
        if (!this->datain || this->datain->atEnd()) {
            bool hasnext = readNextFile();
            if (!hasnext)
                return false;
        }
        processLine();
    }//while there is still to read

    setAllStringMappings();

    printf("Graph Load completed\n");
    printf("Saving %d node attributes and %d edge attributes.\n", this->nodelabel.size(), this->edgelabel.size());
    printf("This file contains %d Nodes and %d Edges.\n", this->graph->numNodes(), this->graph->numEdges());
    fflush(stdout);
    return true;

}

Graph *DataControl::getGraph() {
    return this->graph;
}

void DataControl::writeToFile(string filename) {
    ofstream file;
    file.open(filename.c_str());

    for (Node *node :this->graph->nodes) {
        file << node->id << "\t" << node->scaledpos.x() << "\t" << node->scaledpos.y() << endl;
    }

    file.close();
}

std::string DataControl::graphToCSV() {
    std::ostringstream stream;

    for (Node *node :this->graph->nodes) {
        stream << node->id << ";" << node->scaledpos.x() << ";" << node->scaledpos.y() << endl;
    }

    return stream.str();
}