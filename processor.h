#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "redis_service.h"

class Processor {
public:
    Processor();

    /// Calculates the layout of a single file stored in redis and puts
    /// the resulting layout into redis as well.
    /// After a successful layout calculation, the input file gets gets
    /// removed from redis.
    bool processFile(std::string id);

private:
    RedisService *redis;
};


#endif //PROCESSOR_H
