#-------------------------------------------------
#
# Project created by QtCreator 2015-10-18T15:50:33
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = LayoutCode
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app

mac {
  CONFIG -= app_bundle
  LIBS += -framework OpenCL
  QMAKE_LFLAGS += -framework OpenCL

} else { #assume linux
  LIBS += -lGLU -lOpenCL -lGLEW -lboost_system
  LIBS += -L/usr/local/include/grpc++ -lgrpc++
  LIBS += -L/usr/local/include/google/protobuf -lprotobuf
  LIBS += -L/usr/local/include/redis3m -lredis3m
}

SOURCES += main.cpp \
           layout/*.cpp \
           datacontrol.cpp \
           propertyBag.cpp \
           dago-layout/*.cc \
           redis_service.cpp \
           processor.cpp

HEADERS += node-edge.h \
           layout/*.h \
           datacontrol.h \
           propertyBag.h \
           property.h \
           object.h\
           dago-layout/*.h \
           redis_service.h \
           processor.h
