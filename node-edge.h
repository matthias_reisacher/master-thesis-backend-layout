#ifndef NODE_EDGE_H
#define NODE_EDGE_H

#include <math.h>
#include <stdlib.h>

#include "propertyBag.h"

#include "layout/GM3Interface.h"

class Node;

class Edge : public PropertyBag, public GM3::edge {
public:
    Node *n0, *n1;

    Edge(Node *from, Node *to) : PropertyBag("Edge", "unkown") {
        n0 = from;
        n1 = to;
        newedge = false;
        todelete = false;
    }

    void setEdgeIds(int from, int to) {
        id1 = from;
        id2 = to;
    }

    int expiration;
    bool todelete;
    bool newedge;
};

class Node : public PropertyBag, public GM3::node {
public:
    Node() : PropertyBag("Node", "unkown") {
        weight = 1;
        float theta = fmod(rand(), 2 * M_PI);
        float radius = 1 + rand() * 1.0 / RAND_MAX;
        x = radius * cos(theta);
        y = radius * sin(theta);
        prevx = sprevx = numeric_limits<float>::max();
        prevy = sprevy = numeric_limits<float>::max();
        degree = 0;
        todelete = false;
    }

    GM3::edge *findEdge(unsigned int id) {
        for (unsigned int i = 0; i < edges.size(); i++)
            if (edges[i]->id2 == id)
                return edges[i];

        return 0;
    }

    void setPosition(float x, float y) {
        this->x = x;
        this->y = y;
    }

    bool removeEdge(unsigned int id) {
        for (unsigned int i = 0; i < edges.size(); i++)
            if (edges[i]->id2 == id) {
                edges.erase(edges.begin() + i);
                return true;
            }

        return false;
    }

    float theta, size;
    float weight;
    int expiration;
    float x, y, prevx, prevy, sprevx, sprevy;
    bool todelete;

};


class Graph : public GM3::Graph {

public:
    Graph() : GM3::Graph() { refinementran = false; }

    inline unsigned int numNodes() {
        return nodes.size();
    }

    inline unsigned int numEdges() {
        return edges.size();
    }

    inline GM3::node *getNodeByID(unsigned int id) {
        if (nodemap.find(id) == nodemap.end()) {
            printf("shit hit the fan\n");
            exit(1);
        }
        return (GM3::node *) nodemap[id];
    }

    inline GM3::node *getNodeByInd(unsigned int i) {
        return (GM3::node *) nodes[i];
    }

    inline GM3::edge *getEdge(unsigned int i) {
        return (GM3::edge *) edges[i];
    }

    inline void setNodeByID(unsigned int id, GM3::node *node) {
        nodemap[id] = (Node *) node;
    }

    inline void setNodeByInd(unsigned int ind, GM3::node *node) {
        nodes[ind] = (Node *) node;
    }

    inline void setEdge(unsigned int i, GM3::edge *edge) {
        edges[i] = (Edge *) edge;
    }

    inline bool removeEdge(unsigned int src, unsigned int id) {
        for (unsigned int i = 0; i < edges.size(); i++)
            if (edges[i]->id1 == src && edges[i]->id2 == id) {
                edges.erase(edges.begin() + i);
                nodemap[src]->removeEdge(id);
                return true;
            }

        return false;
    }

    vector<Node *> nodes;
    vector<Edge *> edges;
    map<int, Node *> nodemap;
    bool refinementran;
};


#endif // NODE_EDGE_H
