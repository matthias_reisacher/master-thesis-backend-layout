# Incremental Online Dynamic Graph Layout #

A standalone version of the graph layout from [An Incremental Layout Method for Visualizing Online Dynamic Graphs](http://vidi.cs.ucdavis.edu/Projects/Streaming) paper. The graph layout is a modification of FM3 and novel refinement method. 

Graphs provide a visual means for examining relation data and force-directed methods are often used to lay out graphs for viewing. Making sense of a dynamic graph as it evolves over time is challenging, and previous force-directed methods were designed for static graphs. We present an incremental version of a multilevel multi-pole layout method with a refinement scheme incorporated, which enables us to visualize online dynamic networks while maintaining a mental map of the graph structure.

This repository contains the layout and example code. The layout code can be found in the layout folder. Datacontrol.cpp and Datacontrol.h takes care of loading, populating and outputting the results. The node-edge.h is example code on how to inherit node and edge class from the layout. Lastly, main.cpp demonstrates how to use the layout.

## How do I get set up? ##
### Dependencies ###
Qt:A GUI toolkit. Available on all platforms. 
OpenCl: A low-Level API for executing code on the GPU. Native on Mac. For Windows and Linux, guides are readily available on how to install on the each platform.
### Building code ###
Once QT and OpenCl are installed, go to the directory and run command.

```
#!bash

qmake
make
```
### Running an example ###

An example data set can be found in the "example" folder. To run the code

LayoutCode Folder Folder/output_filname
```
#!bash
LayoutCode example/ output/test
```
Each line of the output file contains tab seperated nodeID, x and y.

```
#!bash

196017	0.162874	0.166038
3886	0.165594	0.17601
503093	0.0702959	0.235229
44984	0.0811345	0.238916
3088059	0.592816	0.954616
372516	0.594198	0.967984
4898287	0	0.456899
355620	0.0164905	0.455673
6903568	0.985479	0.353608
412286	0.973102	0.349709
10437367	0.724966	0.953177
335620	0.728408	0.940017
11746831	0.922221	0.793507
455544	0.918813	0.778642
13793162	0.766709	0.662986
```

### Using Layout on its own ###


```
#!c++
  Graph *g = new Graph();// 1.
 
  //loads the data. An example file is located in "example" folder
  loadData();// 2.

  //initial calculation step
  computeLayout(g); // 3.

  GM3::Parameterization *p = new GM3::Parameterization();// 4.
  p->incremental = true; // 5.

  while(more data incoming){ // 6.
      //compute an incremental layout
      computeLayout(data->getGraph(), p); // 7.

      //Determines which nodes need to read just and moves them
      for(int i = 0 ; i < numiterations; i++) // 8.
        computeOneRefinementStep(data->getGraph());// 9.

      //Renderer or output results // 10.
      data->writeToFile("filename.txt");
  }

```


1. Create Your own Node, Edge, Graph class that inherits from GM3::node, GM3::edge, and GM3::Graph. An example on how to do it can be found in node-edge.h
2. Load the data into the graph. An example of loading a file into the class can be found in datacontrol.cpp::Load.
3. Compute the initial Layout while passing in the graph.
4. If you want to change the parameters or switch to incremental mode, create an instance of Parameterization and set the corresponding variables. The list of variables and description can be found in GM3Interface.h
5. set the incremental boolean to true.
6. Add more data to the graph.
7. run computeLayout again with the Graph and parameters passed in
8. It's recommended to run the refinement step at least few times between each computeLayout
9. run computeOneRefinementStep. The function calls two other public functions that might be a note of interest. computeGraphTension calculates the amount of energy each node has. ComputeOneGraphStep runs 20 iterations of the lowest level of the layout.
10. Lastly either output the results or renderer onto screen. Example of outputting into file can be found in DataControl::writeToFile();