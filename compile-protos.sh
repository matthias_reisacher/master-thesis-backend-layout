#!/usr/bin/env bash
set -o nounset
#set -o xtrace

die() {
    echo "$@" >&2
    exit 1
}

proc_protos() {
    protoc -I=${VOLUME_PROTO} ${VOLUME_PROTO}/${1}/*.proto \
        --cpp_out=${SRC_DIR} \
        --plugin=protoc-gen-grpc=${GRPC_CPP_PLUGIN} \
        --grpc_out=${SRC_DIR}
}

GRPC_CPP_PLUGIN=$(command -v grpc_cpp_plugin)

[[ ! -d ${VOLUME_PROTO} ]] && die 'Error: Proto volume not found!'
[[ -z ${GRPC_CPP_PLUGIN} ]] && die 'Error: gRPC plugin not found!'

cd $(dirname "$0")
SRC_DIR='./'

proc_protos "dago-layout"