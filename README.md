# Introduction
This project is based on the project [Incremental Online Dynamic Graph Layout](https://bitbucket.org/turokhunter/online-dynamic-graph-layout/src/master/) by *Tarik Crnovrsanin*.
In the process of including this project into the DAGO infrastructure, some changes had to be made.
These changes and resulting dependencies are documented here.

## Changes
* Moving the original project into Docker.
* Adding a gRPC WebServer.
* Adding support for Redis.

## Requirements
Due to the usage of *OpenCl* in the original project, the [nvidia-docker 2.0](https://github.com/nvidia/nvidia-docker/wiki/Installation-%28version-2.0%29) driver is needed.
The installation guide is provided on their official github page.

  