#ifndef REDISSERVICE_H
#define REDISSERVICE_H

#include <redis3m/redis3m.hpp>

class RedisService {
public:
    RedisService(std::string host, int port);

    std::string readInputFile(std::string id);

    void writeOutputFile(std::string id, std::string file);

    void deleteInputFile(std::string id);

private:
    redis3m::connection::ptr_t conn;

    std::string createRedisKeyInput(std::string id);

    std::string createRedisKeyOutput(std::string id);
};

#endif //REDISSERVICE_H
