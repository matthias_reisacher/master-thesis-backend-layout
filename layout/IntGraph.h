/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef GM3_GRAPH_H
#define GM3_GRAPH_H

#include "GM3Graph.h"

#include "cvec.h"

#include <string>
#include <vector>
#include <map>

namespace GM3 {

    class KDTree;

    class IntGraph : public Graph {
        friend class KDTree;

    public:
        inline static bool edgeEqual(edge e1, edge e2) {
            return e1.id1 == e2.id1 && e1.id2 == e2.id2;
        }

#ifdef GM3_LOAD_FROM_FILE
        IntGraph(const char *fn=0);
#endif

        IntGraph();

        IntGraph(std::vector <node> n, std::vector <edge> e);

        ~IntGraph();

        //graph access
        inline std::vector <node> &nodesRef() { return nodes; }

        inline std::vector <edge> &edgesRef() { return edges; }

        //functions inherited from GM3::Graph
        inline unsigned int numNodes() { return nodes.size(); }

        inline unsigned int numEdges() { return edges.size(); }

        inline node *getNodeByID(unsigned int i) { return getNodeByInd(IDToInd.at(i)); }

        inline node *getNodeByInd(unsigned int i) { return &nodes.at(i); }

        inline edge *getEdge(unsigned int i) { return &edges.at(i); }

        inline void setNodeByID(unsigned int i, node *n) { setNodeByInd(IDToInd.at(i), n); }

        inline void setNodeByInd(unsigned int i, node *n) { nodes.at(i) = *n; }

        inline void setEdge(unsigned int i, edge *e) { edges.at(i) = *e; }

        inline void nodeAt(int i, node n) { nodes[i] = n; }

        inline node nodeAt(int i) { return nodes.at(i); }

        inline edge edgeAt(int i) { return edges.at(i); }

        inline void getPos(int i, float *p) {
            p[0] = nodes[i].p.x();
            p[1] = nodes[i].p.y();
        }

        inline void setPos(int i, float *p) {
            nodes[i].p.x(p[0]);
            nodes[i].p.y(p[1]);
        }

        void getNodePositions(float *);

        void setNodePositions(float *);

        void getNodeWeights(float *);

        void getEdgeWeights(float *);


        //FIXME: should these be public?
        //node and edge vectors--store associated information
        std::vector <node> nodes;
        std::vector <edge> edges;


        float lowerx, lowery, upperx, uppery;

        float changeimpact;//what is the maxiam change in the graph

        //graph setup
#ifdef GM3_LOAD_FROM_FILE
        void loadFromFile(const char *fn);
#endif

        void loadFromPassed(std::vector <node> &n, std::vector <edge> &e);

        void randomize();

        void sortEdges();


        //rescale the graph to [0,1]
        void rescale(int level);

        void boundingBox(float &x0, float &y0, float &x1, float &y1);

        //Multilevel stuff
        IntGraph *maxIndptSet();

        void setLayoutFromMIS(IntGraph *g, float j);

        void setLayoutFromMISOffset(IntGraph *g);

        //Multipole stuff
        std::vector<unsigned int> buildKDTree();

        std::vector<unsigned int> buildKDTree(unsigned int thresh);

        inline KDTree *tree() { return kd; }

        //handle node and edge renumbering
        void translateEdges(std::vector<unsigned int> &);

        void translateParents(std::vector<unsigned int> &);

    private:
        static const unsigned int buffsize = 1024; //max MatrixMarket line

        static float jitter; //random displacement of nodes in setLayoutfromMIS
        KDTree *kd;

        //general IntGraph loading
        void formatLoadedData();

        void networkAging();

        int findFirstInstance(int id);

        //ID->index table
        static const unsigned int invalidID = 0x1111u;
        std::map<unsigned int, unsigned int> IDToInd;


#ifdef GM3_LOAD_FROM_FILE
        //custom node/link loading
        // Returns 0 on failure, non-zero on success
        int loadNodEdg(std::string fn);
        int loadNodeFile(std::string fn);
        int loadEdgeFile(std::string fn);

        //matrix market loading
        // Returns 0 on failure, non-zero on success
        int loadMTX(std::string fn);
        int processMTXHeader(std::ifstream &);
        int processMTXComments(std::ifstream &);
        int processMTXStats(std::ifstream &, unsigned int &N, unsigned int &E);
        int processMTXPatternLines(std::ifstream &,
                unsigned int N, unsigned int E);
        int processMTXValuedLines(std::ifstream &,
                unsigned int N, unsigned int E);
        void createBlankNodes(unsigned int);
        void skip(std::ifstream &in, char c);
        void fileCleanup(std::ifstream &f);
#endif

        inline void addNode(node n) { nodes.push_back(n); }

        inline void addEdge(edge e) { edges.push_back(e); }
    };

}

#endif
