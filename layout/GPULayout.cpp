/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#include "GPULayout.h"


#include <ctime>
#include <queue>
#include <cmath>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include <cstdio>

#include "timelogger.h"

using namespace std;
using namespace GM3;

#define TIME_TRIALS

//#define TIME_ACC
unsigned int GPULayout::n = 128; //number of work-item per work-group

/*************************/
/*Kernel argument indices*/
enum {
    NODEP_i,
    CSRN_i,
    CSRE_i,
    NODEW_i,
    EDGEW_i,
    NODEMASK_i,
    NODETEMP_i,
    KDNODES_i,
    KDOFFSET_i,
    ETA_i,
    C_i,
    DESLENGTH_i,
    T_i,
    SHAREDPOS_i,
    SHAREDWT_i,
};
/*************************/

/*************************/
/*Kernel Potential Engery argument indices*/
enum {
    PE_NODEP_i,
    PE_CSRN_i,
    PE_CSRE_i,
    PE_NODEW_i,
    PE_EDGEW_i,
    PE_RESULTS_i,
    PE_C_i,
    PE_DESLENGTH_i,
};
/*************************/

#ifdef GM3_LOAD_FROM_FILE
GPULayout::GPULayout(const char *fn):
    Layout(fn),
    context(), devices(NULL), commandQueue(), OCLProg(),
    nodes_b(0), CSRN_b(0), CSRE_b(0), nodeW_b(0), edgeW_b(0), KDNodes_b(0), nodemask_b(0), nodetemp_b(0),
    GPUTmp(), GPUKDNodes(), CSRNodes(), CSREdges()
{
    initOnce();
}
#endif

GPULayout::GPULayout() :
        Layout(),
        context(), devices(NULL), commandQueue(), OCLProg(),
        nodes_b(0), CSRN_b(0), CSRE_b(0), nodeW_b(0), edgeW_b(0), KDNodes_b(0), nodemask_b(0), nodetemp_b(0),
        GPUTmp(), GPUKDNodes(), CSRNodes(), CSREdges(), refinement(false) {
    initOnce();
}

GPULayout::~GPULayout() {
    cleanupCL();
}


void GPULayout::cleanupCL() {
    cleanupCLMem();
    if (stepKern) clReleaseKernel(stepKern);
    if (potentialKern) clReleaseKernel(potentialKern);
    if (OCLProg) clReleaseProgram(OCLProg);
    if (OCLProg2) clReleaseProgram(OCLProg2);
    if (commandQueue) clReleaseCommandQueue(commandQueue);
    if (devices) free(devices);
    if (context) clReleaseContext(context);
}

void GPULayout::cleanupCLMem() {
    if (nodes_b) cluReleaseMemObject(nodes_b);
    if (CSRN_b) cluReleaseMemObject(CSRN_b);
    if (CSRE_b) cluReleaseMemObject(CSRE_b);
    if (nodeW_b) cluReleaseMemObject(nodeW_b);
    if (edgeW_b) cluReleaseMemObject(edgeW_b);
    if (nodemask_b) cluReleaseMemObject(nodemask_b);
    if (nodetemp_b) cluReleaseMemObject(nodetemp_b);
    if (KDNodes_b) cluReleaseMemObject(KDNodes_b);
}

void errNotify(const char *errstr, const void *priv, size_t cb, void *usr) {
    std::cerr << "Error in this context: " << errstr;
}


void GPULayout::loadGraph(IntGraph *g) {
    Layout::loadGraph(g);

    initPerGraph();
}


void GPULayout::initOnce() {
    //get the platform
    unsigned int numPlatforms;
    cluGetPlatformIDs(0, NULL, &numPlatforms);
    cl_platform_id platIDs[numPlatforms];
    cluGetPlatformIDs(numPlatforms, platIDs, NULL);

    //create the platform property list
    cl_context_properties clcxProps[] =
            {
                    CL_CONTEXT_PLATFORM, (cl_context_properties) platIDs[0], 0
            };

    //create an OpenCL context
    context = cluCreateContextFromType(clcxProps, CL_DEVICE_TYPE_GPU,
                                       errNotify, NULL);

    //set up the device and a command queue for it
    size_t ParmDataBytes;
    cluGetContextInfo(context, CL_CONTEXT_DEVICES, 0, NULL,
                      &ParmDataBytes);
    devices = (cl_device_id *) malloc(ParmDataBytes);
    cluGetContextInfo(context, CL_CONTEXT_DEVICES, ParmDataBytes,
                      devices, NULL);

    commandQueue = cluCreateCommandQueue(context, devices[0], 0);
    //CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE);

    //create and compile the program
    char *src[1] = {loadProg("./layout/Multipole.cl")};
    if (src != NULL) {
        OCLProg = cluCreateProgramWithSource(context, 1,
                                             const_cast<const char **>(src), NULL);
        cluBuildProgram(OCLProg, 1, devices, NULL, NULL, NULL);
        delete src[0];
    }

    //get the kernel handle
    stepKern = cluCreateKernel(OCLProg, "stepKern");

    char *src2[1] = {loadProg("./layout/Potential_Energy.cl")};
    if (src2 != NULL) {
        OCLProg2 = cluCreateProgramWithSource(context, 1,
                                              const_cast<const char **>(src2), NULL);
        cluBuildProgram(OCLProg2, 1, devices, NULL, NULL, NULL);
        delete src2[0];
    }

    potentialKern = cluCreateKernel(OCLProg2, "potentialKern");


    KDTree::threshold(n);
}

void GPULayout::initPerGraph() {
    //clear out old buffers
    cleanupCLMem();

    unsigned int N = graphList[0].numNodes();
    unsigned int E = graphList[0].numEdges();

    //allocate node and edge buffers for the GPU
    nodes_b = cluCreateBuffer(context, CL_MEM_READ_WRITE,
                              sizeof(float) * 2 * N, NULL);

    CSRN_b = cluCreateBuffer(context, CL_MEM_READ_ONLY,
                             sizeof(cl_uint) * (N + 1), NULL);
    CSRE_b = cluCreateBuffer(context, CL_MEM_READ_ONLY,
                             sizeof(cl_uint) * E, NULL);

    nodemask_b = cluCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * N, NULL);
    nodetemp_b = cluCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * N, NULL);

    nodeW_b = cluCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * N, NULL);
    edgeW_b = cluCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * E, NULL);

    unsigned int kdsize = 4 * (unsigned int) (N / n) + 1;
    KDNodes_b = cluCreateBuffer(context, CL_MEM_READ_ONLY,
                                sizeof(kdnode) * kdsize, NULL);

    //local buffers to store GPU-formatted data
    GPUTmp.resize(N * 2 > E ? N * 2 : E);
    GPUKDNodes.resize(kdsize);
    CSRNodes.reserve(N + 1);
    CSREdges.reserve(E);
    NodeMask.resize(N);
    NodeTemp.resize(N, 1);

    cluFinish(commandQueue);

    //pointers to global memory
    cluSetKernelArg(stepKern, NODEP_i, sizeof(cl_mem), (void *) &nodes_b);
    cluSetKernelArg(stepKern, CSRN_i, sizeof(cl_mem), (void *) &CSRN_b);
    cluSetKernelArg(stepKern, CSRE_i, sizeof(cl_mem), (void *) &CSRE_b);
    cluSetKernelArg(stepKern, NODEW_i, sizeof(cl_mem), (void *) &nodeW_b);
    cluSetKernelArg(stepKern, EDGEW_i, sizeof(cl_mem), (void *) &edgeW_b);
    cluSetKernelArg(stepKern, NODEMASK_i, sizeof(cl_mem), (void *) &nodemask_b);
    cluSetKernelArg(stepKern, NODETEMP_i, sizeof(cl_mem), (void *) &nodetemp_b);
    cluSetKernelArg(stepKern, KDNODES_i, sizeof(cl_mem), (void *) &KDNodes_b);


    //pointers to local memory
    cluSetKernelArg(stepKern, SHAREDPOS_i, sizeof(float) * 2 * n, NULL);
    cluSetKernelArg(stepKern, SHAREDWT_i, sizeof(float) * n, NULL);
}

void GPULayout::initPerLayout() {
    buildReductions();

    currGraph = graphList.end() - 1;

    cluSetKernelArg(stepKern, ETA_i, sizeof(float), (void *) &eta);
    cluSetKernelArg(stepKern, C_i, sizeof(float), (void *) &repConst);
}

unsigned int GPULayout::initPerLevel() {
    unsigned int g = currGraph - graphList.begin();
    unsigned int steps;
    float d;

    //determine the number of steps
    if (graphList.size() - 1 <= 0) steps = (coarsestSteps + finestSteps) / 2;
    else {
        steps = finestSteps + g * (coarsestSteps - finestSteps) /
                              (graphList.size() - 1);
    }

    //calculate initialT decay - less energy at lower levels
    currT = Layout::initialT / pow(initialTDecay, (float) (graphList.size() - 1 - g));

    //calculate desired length decay - restrict cluster expansion
    d = Layout::desLength / pow(desLengthDecay, (float) (graphList.size() - 1 - g));
    cluSetKernelArg(stepKern, DESLENGTH_i, sizeof(float), (void *) &d);

    //finish all writes
    //cluFinish(commandQueue);

    return steps;
}

void GPULayout::initPerKDTree() {
    //rebuild the CSR
    buildCSR();

    KDTree *kd = currGraph->tree();
    unsigned int N = currGraph->numNodes();
    unsigned int E = currGraph->numEdges();

    //write the offset for the first leaf node
    cl_uint ofs = kd->size() - kd->leaves();
    cluSetKernelArg(stepKern, KDOFFSET_i, sizeof(cl_uint), (void *) &ofs);

    //write everything that might have changed
    convertGraphNodePosToGPU(currGraph->nodesRef(), GPUTmp);
    cluEnqueueWriteBuffer(commandQueue, nodes_b, CL_TRUE, 0,
                          sizeof(cl_float) * N * 2, &(GPUTmp[0]), 0, NULL, NULL);

    cluEnqueueWriteBuffer(commandQueue, CSRN_b, CL_TRUE, 0,
                          sizeof(cl_uint) * CSRNodes.size(), &(CSRNodes[0]), 0, NULL, NULL);

    convertGraphNodeWToGPU(currGraph->nodesRef(), GPUTmp);
    cluEnqueueWriteBuffer(commandQueue, nodeW_b, CL_TRUE, 0,
                          sizeof(cl_float) * N, &(GPUTmp[0]), 0, NULL, NULL);

    cluEnqueueWriteBuffer(commandQueue, nodemask_b, CL_TRUE, 0,
                          sizeof(cl_int) * N, &(NodeMask[0]), 0, NULL, NULL);

    cluEnqueueWriteBuffer(commandQueue, nodetemp_b, CL_TRUE, 0,
                          sizeof(cl_int) * N, &(NodeTemp[0]), 0, NULL, NULL);

    if (E > 0) {
        cluEnqueueWriteBuffer(commandQueue, CSRE_b, CL_TRUE, 0,
                              sizeof(cl_uint) * CSREdges.size(), &(CSREdges[0]), 0, NULL, NULL);

        convertGraphEdgeWToGPU(currGraph->edgesRef(), GPUTmp);
        cluEnqueueWriteBuffer(commandQueue, edgeW_b, CL_TRUE, 0,
                              sizeof(cl_float) * E, &(GPUTmp[0]), 0, NULL, NULL);
    }

    convertKDTree(currGraph->tree(), GPUKDNodes);
    cluEnqueueWriteBuffer(commandQueue, KDNodes_b, CL_TRUE, 0,
                          sizeof(kdnode) * GPUKDNodes.size(), &(GPUKDNodes[0]), 0, NULL, NULL);
}

void GPULayout::initPerStep() {
    cluSetKernelArg(stepKern, T_i, sizeof(cl_float), (void *) &currT);
}


void GPULayout::computeLayout() {
#ifdef TIME_ACC
    TimeLogger::Instance()->push("Running layout.");
    TimeLogger::Instance()->start();
#endif


    initPerLayout();

#ifdef TIME_ACC
    TimeLogger::Instance()->markIt("init per layout: ");
#endif

    //  if(Layout::incremental)
    //   computeTension();

    for (unsigned int i = 0; i < graphList.size(); i++) {
        std::cout << "Processing graph " << i << " of " << graphList.size() << std::endl;

        layoutLevel();

        //set up the next graph if it exists
        if (currGraph > graphList.begin()) {
            currGraph--;
            if (incremental)
                currGraph->setLayoutFromMISOffset(&*(currGraph + 1));
            else
                currGraph->setLayoutFromMIS(&*(currGraph + 1), initialT / 10.f);
        }
    }


    //wait for everything in the queue to finish
    cluFinish(commandQueue);

#ifdef TIME_ACC
    TimeLogger::Instance()->markIt("GPU layout time: ");
    TimeLogger::Instance()->pop("Computing Layout took: ");
#endif

    //rescale all levels
    for (unsigned int i = 0; i < graphList.size(); i++) graphList[i].rescale(i);
}


void GPULayout::layoutLevel() {
    if (currGraph->numNodes() <= 0) return;

#ifndef TIME_TRIALS
    printf("Impact:%f\n",currGraph->changeimpact);
#endif

    //if(incremental && currGraph->changeimpact < 0.01)
    // return;

    unsigned int steps = initPerLevel();


#ifdef TIME_ACC //print notice and start the clock
    //unsigned int g = currGraph - graphList.begin();
    //cout << "Level: " << g << ", Nodes: " << currGraph->numNodes() <<
    //    ", Edges: " << currGraph->numEdges() << ", Steps: " << steps << flush<<endl;
    TimeLogger::Instance()->push("Running layout level.");
    TimeLogger::Instance()->start();
    double timeperlayoutstep = 0;
    double timeperkdbuild = 0;
#endif
    if (incremental)
        buildKDTree();

    unsigned int lastKDStep = 0;

    //Actually layout the level
    for (unsigned int i = 0; i < steps; i++) {

        if (!incremental && KDRebuildStep(i, lastKDStep)) {
            lastKDStep = i;
#ifdef TIME_ACC
            QElapsedTimer * timer =  new QElapsedTimer();
            timer->start();
#endif
            buildKDTree();
#ifdef TIME_ACC
            timeperkdbuild += timer->nsecsElapsed();
#endif
        }
#ifdef TIME_ACC
        QElapsedTimer * timer =  new QElapsedTimer();
        timer->start();
#endif
        layoutStep();

        //reduce temperature
        currT = currT * lambda;

        //finish operations
        cluFinish(commandQueue);
#ifdef TIME_ACC
        timeperlayoutstep +=  timer->nsecsElapsed();
#endif
        //get ready to set up the next KD tree
        cluEnqueueReadBuffer(commandQueue, nodes_b, CL_TRUE, 0,
                             sizeof(cl_float) * currGraph->numNodes() * 2, &(GPUTmp[0]),
                             0, NULL, NULL);
        convertGraphNodePosToCPU(GPUTmp, currGraph->nodesRef());
    }

#ifdef TIME_ACC //stop the clock and return the comp time
    cluFinish(commandQueue);
    unsigned int g = currGraph - graphList.begin();

    TimeLogger::Instance()->markIt("Level "+QString().number(g)+" sum kd build: ", timeperkdbuild/1000000);
    TimeLogger::Instance()->markIt("Level "+QString().number(g)+" sum layoutstep: ", timeperlayoutstep/1000000);
    TimeLogger::Instance()->markIt("Layout level "+QString().number(g)+" times: ");
    TimeLogger::Instance()->pop("Computing a level of the layout took: ");
#endif
}


void GPULayout::layoutStep() {
    initPerStep();

    size_t gwork[1] = {0};
    size_t lwork[1] = {0};
    int n = currGraph->tree()->maxLeaf();

    gwork[0] = (size_t)(currGraph->tree()->leaves() * n);
    lwork[0] = (size_t)(n);
    cluEnqueueNDRangeKernel(commandQueue, stepKern, 1, NULL, gwork, lwork,
                            0, NULL, NULL);

#ifdef TIME_ACC
    //cout << '.' << flush;
#endif
}


void GPULayout::computePotentialEnergy() {



    //create clu buffers
    pe_results_b = cluCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_float) * currGraph->numNodes(), NULL);

    cluFinish(commandQueue);

    //set points to global memory
    cluSetKernelArg(potentialKern, PE_NODEP_i, sizeof(cl_mem), (void *) &nodes_b);
    cluSetKernelArg(potentialKern, PE_CSRN_i, sizeof(cl_mem), (void *) &CSRN_b);
    cluSetKernelArg(potentialKern, PE_CSRE_i, sizeof(cl_mem), (void *) &CSRE_b);
    cluSetKernelArg(potentialKern, PE_NODEW_i, sizeof(cl_mem), (void *) &nodeW_b);
    cluSetKernelArg(potentialKern, PE_EDGEW_i, sizeof(cl_mem), (void *) &edgeW_b);


    cluSetKernelArg(potentialKern, PE_RESULTS_i, sizeof(cl_mem), (void *) &pe_results_b);
    PE_results.resize(currGraph->numNodes());

    cluSetKernelArg(potentialKern, PE_C_i, sizeof(float), (void *) &repConst);

    unsigned int g = currGraph - graphList.begin();

    float d = Layout::desLength / pow(desLengthDecay, (float) (graphList.size() - 1 - g));


    cluSetKernelArg(potentialKern, PE_DESLENGTH_i, sizeof(float), (void *) &d);


    //call the function

    size_t gwork[1] = {0};
    //size_t lwork[1] = {0};

    gwork[0] = (size_t)(currGraph->numNodes());
    //lwork[0] = (size_t)(32);
    cluEnqueueNDRangeKernel(commandQueue, potentialKern, 1, NULL, gwork, NULL,
                            0, NULL, NULL);

    cluFinish(commandQueue);

    //read from buffer

    cluEnqueueReadBuffer(commandQueue, pe_results_b, CL_TRUE, 0, sizeof(cl_float) * currGraph->numNodes(),
                         &(PE_results[0]), 0, NULL, NULL);


}

std::vector<float> &GPULayout::computeGraphTension() {
    computePotentialEnergy();
    return PE_results;
}


void GPULayout::computeOneGraphStep(bool fullrelayout, map<int, bool> list) {

    if (fullrelayout) {
        for (unsigned int n = 0; n < currGraph->numNodes(); n++) {
            currGraph->getNodeByInd(n)->newnode = true;
        }
    } else {
        for (unsigned int n = 0; n < currGraph->numNodes(); n++) {
            node *node = currGraph->getNodeByInd(n);
            if (list[node->id]) {
                node->newnode = true;
            } else
                node->newnode = false;
        }

    }
    incremental = false;
    refinement = true;

    buildKDTree();

    currT = 1;//0.2
    for (int i = 0; i < 50; i++) {
        layoutStep();

        //finish operations
        cluFinish(commandQueue);
        cluEnqueueReadBuffer(commandQueue, nodes_b, CL_TRUE, 0,
                             sizeof(cl_float) * currGraph->numNodes() * 2, &(GPUTmp[0]),
                             0, NULL, NULL);
        convertGraphNodePosToCPU(GPUTmp, currGraph->nodesRef());
    }

    refinement = false;
    incremental = true;
    currGraph->rescale(0);

}


//Better function: the number of iterations between rebuilds increases with the
//logarithm(base 2/3*lambda) of the last rebuild step.
bool GPULayout::KDRebuildStep(unsigned int s, unsigned int s0) {
    return (float) (s - s0) > -1.f * log((float) s0) / log(lambda * 2.f / 3.f);
}

void GPULayout::buildKDTree() {
    Layout::buildKDTree();

    initPerKDTree();
}


GPULayout::kdnode GPULayout::convertKDNode(KDTree::kdnode *kn) {
    kdnode gn;

    gn.x = kn->p.x();
    gn.y = kn->p.y();

    gn.d = kn->d;
    gn.w = kn->w;
    gn.s = kn->s;
    gn.t = kn->t;

    gn.l = -1;
    gn.r = -1;

    return gn;
}

//Re-storing algorithm
// Start with the root node in the queue
// Until the queue is empty:
// 1. Pop node from the queue
// 2. Add left and right child to the queue
// 3. Place converted node in vector
// 4. Calculate new left and right pointers
void GPULayout::convertKDTree(KDTree *kd,
                              std::vector <GPULayout::kdnode> &kdnodes) {
    if (!kd) return; //don't process a NULL pointer

    kdnodes.clear(); //make sure the KD tree buffer is empty

    std::queue < KDTree::kdnode * > levelOrderQ;
    std::queue <kdnode> leafQ;
    KDTree::kdnode *n;
    unsigned int loff = kd->size() - kd->leaves();

    n = kd->kdroot();
    levelOrderQ.push(n); // Start with the root node in the queue
    while (!levelOrderQ.empty()) { // Until the queue is empty
        // 1. Pop node from the queue
        n = levelOrderQ.front();
        levelOrderQ.pop();

        // 2. Place converted node in vector
        kdnodes.push_back(convertKDNode(n));

        // 3. Handle left child
        if (n->l) {
            if (n->l->l && n->l->r) { //interior
                // 3.1 Add left child to the queue
                levelOrderQ.push(n->l);

                // 3.2 Calculate new left pointer
                kdnodes.back().l = kdnodes.size() + levelOrderQ.size() - 1;
            } else { //leaf
                // 3.1 Add left child to leaf queue
                leafQ.push(convertKDNode(n->l));

                // 3.2 Calculate new left pointer
                kdnodes.back().l = loff + leafQ.size() - 1;
            }
        }

        // 4. Handle right child
        if (n->r) {
            if (n->r->l && n->r->r) { //interior
                // 4.1 Add right child to the queue
                levelOrderQ.push(n->r);

                // 4.2 Calculate new right pointer
                kdnodes.back().r = kdnodes.size() - 1 + levelOrderQ.size();
            } else { //leaf
                // 4.1 Add right child to leaf queue
                leafQ.push(convertKDNode(n->r));

                // 4.2 Calculate new right pointer
                kdnodes.back().r = loff + leafQ.size() - 1;
            }
        }
    }

    // 5. Add the leaves at the end of the list
    while (!leafQ.empty()) {
        kdnodes.push_back(leafQ.front());
        leafQ.pop();
    }

    rStacklessConv(kdnodes, 0, -1);
}

//Stackless traversal conversion
// Start at root
// A pointer for the right side, "R," is passed in
// 1. Call recursively on node->l with R = node->r
// 2. Call recursively on node->r with R = node->r
// 3. Set node->r = R
void GPULayout::rStacklessConv(std::vector <GPULayout::kdnode> &t, int c, int R) {
    if (t[c].l > 0) rStacklessConv(t, t[c].l, t[c].r);
    if (t[c].r > 0) rStacklessConv(t, t[c].r, R);
    t[c].r = R;
}


void GPULayout::buildCSR() {
    currGraph->sortEdges();

    CSRNodes.clear();
    CSREdges.clear();

    unsigned int edgeIndex = 0;

    for (unsigned int n = 0; n < currGraph->numNodes(); n++) {
        CSRNodes.push_back(edgeIndex);
        if (currGraph->getNodeByInd(n)->newnode) {
            NodeMask[n] = 1;
            if (incremental)
                NodeTemp[n] = currGraph->getNodeByInd(n)->changeweight / currGraph->getNodeByInd(n)->weight;
            else if (refinement) {
                if (currGraph->getNodeByInd(n)->edges.size()) {
                    NodeTemp[n] = 1.0 / currGraph->getNodeByInd(n)->edges.size();
                } else {
                    NodeTemp[n] = 1.0;
                }
            } else
                NodeTemp[n] = 1;
        } else {
            NodeMask[n] = 0;
        }
        //if(NodeMask[n])
        //printf("Node:%d temp:%f\n",n, NodeTemp[n]);
        while (edgeIndex < currGraph->numEdges() &&
               currGraph->edgeAt(edgeIndex).id1 == n) {
            CSREdges.push_back(currGraph->edgeAt(edgeIndex++).id2);
        }
    }
    //printf("\n\n");
    //add a fake node to mark the end of the edge list
    CSRNodes.push_back(currGraph->numEdges());
}


void GPULayout::convertGraphNodePosToGPU(std::vector <node> &g,
                                         std::vector<float> &p) {
    for (unsigned int i = 0; i < g.size(); i++) {
        p[i * 2 + 0] = g[i].p.x();
        p[i * 2 + 1] = g[i].p.y();
    }
}

void GPULayout::convertGraphNodePosToCPU(std::vector <cl_float> &p,
                                         std::vector <node> &g) {
    for (unsigned int i = 0; i < g.size(); i++) {
        g[i].p.x(p[i * 2 + 0]);
        g[i].p.y(p[i * 2 + 1]);
    }
}

void GPULayout::convertGraphNodeWToGPU(std::vector <node> &g,
                                       std::vector <cl_float> &w) {
    for (unsigned int i = 0; i < g.size(); i++) {
        w[i] = g[i].weight;
    }
}

void GPULayout::convertGraphEdgeWToGPU(std::vector <edge> &g,
                                       std::vector <cl_float> &w) {
    for (unsigned int i = 0; i < g.size(); i++) {
        w[i] = g[i].weight;
    }
}
