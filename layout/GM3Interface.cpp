/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#include "GM3Interface.h"

#include "timelogger.h"

#include <map>
#include <algorithm>
#include <cstdio>
#include <queue>
#include <math.h>

//#define TIME_ACC
namespace GM3 {
    const float eta = 0.000001f; //softening factor to avoid divide by 0
    const float omega = 0.5f; //placeholder; actual error computer from err


    void computeLayout(GM3::Graph *G, IntGraph *I) {
        G->layout->loadGraph(I);

        if (G->layout->incremental) {
            G->getGraph(0)->lowerx = G->lowerx;
            G->getGraph(0)->lowery = G->lowery;

            G->getGraph(0)->upperx = G->upperx;
            G->getGraph(0)->uppery = G->uppery;
        }

#ifdef TIME_ACC
        TimeLogger::Instance()->markIt("loadGraph time: ");
#endif

        //do the layout
        G->layout->computeLayout();

        //layout
    }

    IntGraph sanitizeInput(Graph *G) {

#ifdef TIME_ACC
        TimeLogger::Instance()->push("Sanitize Input");
        TimeLogger::Instance()->start();
#endif

        IntGraph I;


        //vectorize the nodes and edges
        std::vector <node> nodes;
        nodes.reserve(G->numNodes());
        for (unsigned int i = 0; i < G->numNodes(); i++) {
            G->getNodeByID(G->getNodeByInd(i)->id);
            nodes.push_back(*(G->getNodeByInd(i)));
        }

        std::vector <edge> edges;
        edges.reserve(G->numEdges());
        for (unsigned int i = 0; i < G->numEdges(); i++) {
            edges.push_back(*(G->getEdge(i)));
        }

        //create node ID->index translation table

#ifdef TIME_ACC
        TimeLogger::Instance()->markIt("pushing graph time: ");
#endif


        I.loadFromPassed(nodes, edges);

#ifdef TIME_ACC
        TimeLogger::Instance()->markIt("Load from passed time: ");
        TimeLogger::Instance()->pop("Santizing data took: ");
#endif

        return I;
    }

    IntGraph sanitize(GM3::Graph *G, Parameterization *P) {
        //static GPULayout layout = GPULayout();
        if (!G->layout)
            G->layout = new GM3::GPULayout();

        if (P == 0)
            P = new Parameterization();

        //copy the parameterization into layout
        G->layout->repConst = P->repConst;
        G->layout->desLength = P->desLength;
        G->layout->initialT = P->initialT;
        G->layout->lambda = P->lambda;
        G->layout->coarseningThresh = P->coarseningThresh;
        G->layout->coarsestSteps = P->coarsestSteps;
        G->layout->finestSteps = P->finestSteps;
        G->layout->initialTDecay = P->initialTDecay;
        G->layout->desLengthDecay = P->desLengthDecay;
        G->layout->theta = P->theta;
        G->layout->err = P->err;
        G->layout->omega = omega;
        G->layout->eta = eta;
        G->layout->incremental = P->incremental;
        G->layout->graphlevel = P->graphlevel;

#ifdef TIME_ACC
        std::cout<<"Computing FM3"<<std::endl;
        TimeLogger::Instance()->start();
#endif

        //sanitize the graph and set it in the layout
        return sanitizeInput(G);
    }

    void computeLayout(GM3::Graph *G, Parameterization *P) {
        //static GPULayout layout = GPULayout();
        if (!G->layout)
            G->layout = new GM3::GPULayout();

        if (P == 0)
            P = new Parameterization();

        //copy the parameterization into layout
        G->layout->repConst = P->repConst;
        G->layout->desLength = P->desLength;
        G->layout->initialT = P->initialT;
        G->layout->lambda = P->lambda;
        G->layout->coarseningThresh = P->coarseningThresh;
        G->layout->coarsestSteps = P->coarsestSteps;
        G->layout->finestSteps = P->finestSteps;
        G->layout->initialTDecay = P->initialTDecay;
        G->layout->desLengthDecay = P->desLengthDecay;
        G->layout->theta = P->theta;
        G->layout->err = P->err;
        G->layout->omega = omega;
        G->layout->eta = eta;
        G->layout->incremental = P->incremental;
        G->layout->graphlevel = P->graphlevel;

#ifdef TIME_ACC
        std::cout<<"Computing FM3"<<std::endl;
        TimeLogger::Instance()->start();
#endif

        //sanitize the graph and set it in the layout
        IntGraph I = sanitizeInput(G);


#ifdef TIME_ACC
        TimeLogger::Instance()->markIt("sanitize input time: ");
#endif

        G->layout->loadGraph(&I);

        if (G->layout->incremental) {
            G->getGraph(0)->lowerx = G->lowerx;
            G->getGraph(0)->lowery = G->lowery;

            G->getGraph(0)->upperx = G->upperx;
            G->getGraph(0)->uppery = G->uppery;
        }


#ifdef TIME_ACC
        TimeLogger::Instance()->markIt("loadGraph time: ");
#endif

        //do the layout
        G->layout->computeLayout();

#ifdef TIME_ACC
        TimeLogger::Instance()->markIt("Layout Computed in: ");
#endif

        convertBack(G);

#ifdef TIME_ACC
        TimeLogger::Instance()->markIt("tranfer over: ");
        TimeLogger::Instance()->outputToScreen();
#endif
        //layout
    }

    void convertBack(GM3::Graph *G) {

        //copy node positions back to graph
        for (unsigned int i = 0; i < G->layout->graph(0)->numNodes(); i++) {
            node *n = G->layout->graph(0)->getNodeByInd(i);
            G->getNodeByID(n->id)->p = n->p;
            G->getNodeByID(n->id)->scaledpos = n->scaledpos;
            G->getNodeByID(n->id)->laid = true;
        }

        //sets any new node or edge to false;
        for (unsigned int i = 0; i < G->numNodes(); ++i) {
            G->getNodeByInd(i)->newnode = false;
            G->getNodeByInd(i)->changenode = false;
            G->getNodeByInd(i)->changeweight = 0;
        }
        for (unsigned int i = 0; i < G->numEdges(); ++i)
            G->getEdge(i)->newedge = false;

        if (!G->layout->incremental) {
            G->lowerx = G->getGraph(0)->lowerx;
            G->lowery = G->getGraph(0)->lowery;

            G->upperx = G->getGraph(0)->upperx;
            G->uppery = G->getGraph(0)->uppery;
        }
    }


    void computeGraphTension(Graph *G) {
        std::vector<float> results = G->layout->computeGraphTension();
        for (unsigned int i = 0; i < results.size(); i++) {
            G->layout->graph(0)->getNodeByInd(i)->energy = results[i];
        }
    }

    void computeOneGraphStep(Graph *G, bool fullrelayout, std::map<int, bool> list) {
        G->layout->computeOneGraphStep(fullrelayout, list);
        convertBack(G);
    }

    void computeOneRefinementStep(Graph *G) {

        computeGraphTension(G);

        std::map<int, bool> allowmovement;
        std::map < int, Tension * > tensions;

        for (int i = 0, size = G->layout->graph(0)->nodes.size(); i < size; i++) {
            GM3::node *node = &G->layout->graph(0)->nodes[i];
            tensions[node->id] = new Tension(node->energy);
        }

        int degree = 2;
        std::queue < node * > forces;
        std::map < node * , bool > visited;

        for (int i = 0, size = G->layout->graph(0)->nodes.size(); i < size; i++) {
            GM3::node *node = &(G->layout->graph(0)->nodes[i]);
            visited.clear();
            forces.push(node);
            tensions[node->id]->degree = 0;
            tensions[node->id]->cost = tensions[node->id]->distforce;

            while (!forces.empty()) {
                GM3::node *headnode = forces.front();
                forces.pop();
                if (tensions[headnode->id]->degree <= degree) {
                    for (unsigned int j = 0; j < headnode->edges.size(); j++) {
                        GM3::node *node2 = G->getNodeByID(headnode->edges[j]->id2);
                        if (visited.find(node2) == visited.end()) {
                            visited[node2] = true;
                            float additiveforce = tensions[headnode->id]->cost * 1.0 / headnode->edges.size();

                            if (tensions.find(node2->id) == tensions.end())
                                tensions[node2->id] = new Tension(0);

                            tensions[node2->id]->cost = additiveforce;
                            tensions[node2->id]->degree = tensions[headnode->id]->degree + 1;
                            forces.push(node2);
                            node2->energy += additiveforce;
                        }
                    }
                }
            }
        }

        float total = 0;
        for (int i = 0, size = G->layout->graph(0)->nodes.size(); i < size; i++) {
            GM3::node *node = &G->layout->graph(0)->nodes[i];
            total += node->energy;
            // isMinMax("Energy", log(node->energy));
            // setPropertyFloat(node, "Energy", log(node->energy));
            //if(node->energy != 0)
            //node->energy = log(node->energy);
        }

        float mean = total / G->layout->graph(0)->nodes.size();
        float cutoff = 0.98;//0.6

        for (int i = 0, size = G->layout->graph(0)->nodes.size(); i < size; i++) {
            GM3::node *node = &G->layout->graph(0)->nodes[i];
            //node->weight = g->layout->graph(0)->nodes[i].edges.size()/2.0;
            if (fabs(node->energy - mean) / mean > cutoff) {
                allowmovement[node->id] = true;
            } else
                allowmovement[node->id] = false;
        }
        computeOneGraphStep(G, false, allowmovement);


    }


// Simple constructors for the Parameterization, node, and edge structs
    Parameterization::Parameterization(float rc, float dl, float it, float l,
                                       unsigned int ct, unsigned int cs, unsigned int fs,
                                       float itd, float dld, float t, float e, bool ag, bool inc, int graphlvl) :
            repConst(rc), desLength(dl), initialT(it), lambda(l),
            coarseningThresh(ct), coarsestSteps(cs), finestSteps(fs),
            initialTDecay(itd), desLengthDecay(dld), theta(t), err(e), aging(ag), incremental(inc),
            graphlevel(graphlvl) {}


}




