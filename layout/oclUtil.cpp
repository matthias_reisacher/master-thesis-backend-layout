/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#include "oclUtil.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

void _cluGetPlatformIDs(const char *fi, int li, cl_uint ne, cl_platform_id *p,
                        cl_uint *np) {
    cl_int err = clGetPlatformIDs(ne, p, np);
    if (err == CL_SUCCESS);
    return;

    std::cerr << "Error getting platform IDs in " <<
              fi << " on line " << li << ": ";

    parseErr(err);
    return;
}

void _cluPrintPlatformVersion(const char *fi, int li, cl_platform_id p) {
    char buf[1024] = {0};

    cl_int err = clGetPlatformInfo(p, CL_PLATFORM_VERSION, sizeof(char) * 1024,
                                   buf, NULL);

    if (err == CL_SUCCESS) {
        std::cout << buf << std::endl;
        return;
    }

    std::cerr << "Error reading platform version in " << fi << " on line "
              << li << ": ";

    parseErr(err);
}

cl_context _cluCreateContextFromType(const char *fi, int li,
                                     cl_context_properties *clcp, cl_device_type cldt,
                                     void (*clcb)(const char *, const void *, size_t, void *),
                                     void *clud) {
    cl_int err;
    cl_context cxt = clCreateContextFromType(clcp, cldt, clcb, clud, &err);
    if (err == CL_SUCCESS) return cxt;

    std::cerr << "Error creating context in " <<
              fi << " on line " << li << ": ";

    parseErr(err);
    return cxt;
}

void _cluGetContextInfo(const char *fi, int li, cl_context clct,
                        cl_context_info clci, size_t clsz, void *clvl, size_t *clsr) {

    cl_int err = clGetContextInfo(clct, clci, clsz, clvl, clsr);
    if (err == CL_SUCCESS) return;

    std::cerr << "Error getting context info in " <<
              fi << " on line " << li << ": ";
    parseErr(err);
}

cl_command_queue _cluCreateCommandQueue(const char *fi, int li, cl_context clct,
                                        cl_device_id clid, cl_command_queue_properties clpr) {
    cl_int err;
    cl_command_queue clcq = clCreateCommandQueue(clct, clid, clpr, &err);
    if (err == CL_SUCCESS) return clcq;

    std::cerr << "Error creating command queue in " <<
              fi << " on line " << li << ": ";
    parseErr(err);
    return clcq;
}

cl_program _cluCreateProgramWithSource(const char *fi, int li, cl_context clct,
                                       cl_uint clas, const char **clsr, const size_t *clls) {
    cl_int err;
    cl_program clpr = clCreateProgramWithSource(clct, clas, clsr, clls, &err);
    if (err == CL_SUCCESS) return clpr;

    std::cerr << "Error loading program source in " <<
              fi << " on line " << li << ": ";
    parseErr(err);
    return clpr;
}

char *loadProg(const char *filename) {
    FILE *fp;
    int count = 0;
    char *src = NULL;

    std::string fn = std::string(filename);

    if (fn.length() > 0) {
        fp = fopen(fn.c_str(), "rt");

        if (fp != NULL) {
            fseek(fp, 0, SEEK_END);
            count = ftell(fp);
            rewind(fp);

            if (count > 0) {
                src = new char[count + 1];
                count = fread(src, sizeof(char), count, fp);
                src[count] = '\0';
            }
            fclose(fp);
        } else {
            std::cerr << "Failure!\n" << "\tCould not open file \"" <<
                      fn << "\"" << std::endl;
        }
    } else {
        std::cerr << "Failure\n" << "\tNull file name." << std::endl;
    }

    return src;
}

void checkProgBuildLog(cl_program prog, cl_device_id did) {
    //get log size
    int logSize = 0;
    clGetProgramBuildInfo(prog, did, CL_PROGRAM_BUILD_LOG,
                          0, NULL, (size_t * ) & logSize);
    char log[logSize];
    clGetProgramBuildInfo(prog, did, CL_PROGRAM_BUILD_LOG,
                          logSize, (void *) log, NULL);

    std::cout << "\n" << log << "\n" << std::endl;
}

void _cluBuildProgram(const char *fi, int li, cl_program clpr, cl_uint clnd,
                      const cl_device_id *cldl, const char *clop,
                      void (*clcb)(cl_program, void *), void *clud) {
    cl_int err = clBuildProgram(clpr, clnd, cldl, clop, clcb, clud);
    if (err == CL_SUCCESS) return;

    std::cerr << "Error building program in " <<
              fi << " on line " << li << ": ";
    parseErr(err);

    checkProgBuildLog(clpr, cldl[0]);
}

cl_mem _cluCreateBuffer(const char *fi, int li, cl_context clct,
                        cl_mem_flags clmf, size_t clsz, void *clhp) {
    cl_int err;
    cl_mem buf = clCreateBuffer(clct, clmf, clsz, clhp, &err);
    if (err == CL_SUCCESS) return buf;

    std::cerr << "Error creating a buffer in " <<
              fi << " on line " << li << ": ";
    parseErr(err);

    return buf;
}

cl_mem _cluCreateImage2D(const char *fi, int li, cl_context clct,
                         cl_mem_flags clmf, const cl_image_format *imgf, size_t iw, size_t ih,
                         size_t iptch, void *clhp) {
    cl_int err;
    cl_mem img = clCreateImage2D(clct, clmf, imgf, iw, ih, iptch, clhp, &err);
    if (err == CL_SUCCESS) return img;

    std::cerr << "Error creating a buffer in " <<
              fi << " on line " << li << ": ";
    parseErr(err);

    return img;
}

void _cluReleaseMemObject(const char *fi, int li, cl_mem clmo) {
    cl_int err = clReleaseMemObject(clmo);

    if (err == CL_SUCCESS) return;

    std::cerr << "Error releasing CL memory object in " <<
              fi << " on line " << li << ": ";

    parseErr(err);
}


void _cluEnqueueWriteBuffer(const char *fi, int li, cl_command_queue clcq,
                            cl_mem clbf, cl_bool clbl, size_t clof, size_t clsz, const void *clhp,
                            cl_uint clne, const cl_event *clew, cl_event *clev) {
    cl_int err = clEnqueueWriteBuffer(clcq, clbf, clbl, clof, clsz, clhp,
                                      clne, clew, clev);
    if (err == CL_SUCCESS) return;

    std::cerr << "Error writing buffer in " <<
              fi << " on line " << li << ": ";
    parseErr(err);
}

void _cluEnqueueReadBuffer(const char *fi, int li, cl_command_queue clcq,
                           cl_mem clbf, cl_bool clbl, size_t clof, size_t clsz, void *clhp,
                           cl_uint clne, const cl_event *clew, cl_event *clev) {
    cl_int err = clEnqueueReadBuffer(clcq, clbf, clbl, clof, clsz, clhp,
                                     clne, clew, clev);
    if (err == CL_SUCCESS) return;

    std::cerr << "Error writing buffer in " <<
              fi << " on line " << li << ": ";
    parseErr(err);
}

cl_kernel _cluCreateKernel(const char *fi, int li, cl_program clpr,
                           const char *clkn) {
    cl_int err;
    cl_kernel kern = clCreateKernel(clpr, clkn, &err);
    if (err == CL_SUCCESS) return kern;

    std::cerr << "Error creating kernel in " <<
              fi << " on line " << li << ": ";
    parseErr(err);

    return kern;
}

void _cluSetKernelArg(const char *fi, int li, cl_kernel clkn, cl_uint clan,
                      size_t clsz, const void *clav) {
    cl_int err = clSetKernelArg(clkn, clan, clsz, clav);
    if (err == CL_SUCCESS) return;

    std::cerr << "Error setting kernel argument in " <<
              fi << " on line " << li << ": ";
    parseErr(err);
}

void _cluEnqueueNDRangeKernel(const char *fi, int li, cl_command_queue clcq,
                              cl_kernel clkn, cl_uint cldm, const size_t *clof, const size_t *clgw,
                              const size_t *cllw, cl_uint clne, const cl_event *clew, cl_event *clev) {
    cl_int err = clEnqueueNDRangeKernel(clcq, clkn, cldm, clof, clgw, cllw,
                                        clne, clew, clev);
    if (err == CL_SUCCESS) return;

    std::cerr << "Error queueing kernel in " <<
              fi << " on line " << li << ": ";

    parseErr(err);

    exit(1);
}

void _cluWaitForEvents(const char *fi, int li,
                       cl_uint num, const cl_event *list) {
    cl_int err = clWaitForEvents(num, list);

    if (err == CL_SUCCESS) return;

    std::cerr << "Error queueing event wait in " <<
              fi << " on line " << li << ": ";

    parseErr(err);

    exit(1);
}

void _cluFinish(const char *fi, int li, cl_command_queue cq) {
    cl_int err = clFinish(cq);

    if (err == CL_SUCCESS) return;

    std::cerr << "Error finishing queued events in " <<
              fi << " on line " << li << ": ";

    parseErr(err);

    exit(1);
}

void parseErr(cl_int err) {
    switch (err) {
        case CL_SUCCESS:
            return;
        case CL_DEVICE_NOT_FOUND:
            std::cerr << "Device not found.";
            break;
        case CL_DEVICE_NOT_AVAILABLE:
            std::cerr << "Device not available.";
            break;
        case CL_COMPILER_NOT_AVAILABLE:
            std::cerr << "Compiler not available.";
            break;
        case CL_MEM_OBJECT_ALLOCATION_FAILURE:
            std::cerr << "Memory object allocation failure.";
            break;
        case CL_OUT_OF_RESOURCES:
            std::cerr << "Out of resources.";
            break;
        case CL_OUT_OF_HOST_MEMORY:
            std::cerr << "Out of host memory.";
            break;
        case CL_PROFILING_INFO_NOT_AVAILABLE:
            std::cerr << "Profiling info not available.";
            break;
        case CL_MEM_COPY_OVERLAP:
            std::cerr << "Mem copy overlap.";
            break;
        case CL_IMAGE_FORMAT_MISMATCH:
            std::cerr << "Image format mismatch.";
            break;
        case CL_IMAGE_FORMAT_NOT_SUPPORTED:
            std::cerr << "Image format not supported.";
            break;
        case CL_BUILD_PROGRAM_FAILURE:
            std::cerr << "Build program failure.";
            break;
        case CL_MAP_FAILURE:
            std::cerr << "Map failure.";
            break;
            /*case CL_MISALIGNED_SUB_BUFFER_OFFSET:
                std::cerr << "Misaligned sub-buffer offset."; break;*/
            /*case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
                std::cerr << "Execution status error for wait list events.";break;*/
        case CL_INVALID_VALUE:
            std::cerr << "Invalid value.";
            break;
        case CL_INVALID_DEVICE_TYPE:
            std::cerr << "Invalid device type.";
            break;
        case CL_INVALID_PLATFORM:
            std::cerr << "Invalid platform.";
            break;
        case CL_INVALID_DEVICE:
            std::cerr << "Invalid device.";
            break;
        case CL_INVALID_CONTEXT:
            std::cerr << "Invalid context.";
            break;
        case CL_INVALID_QUEUE_PROPERTIES:
            std::cerr << "Invalid queue properties.";
            break;
        case CL_INVALID_COMMAND_QUEUE:
            std::cerr << "Invalid command queue.";
            break;
        case CL_INVALID_HOST_PTR:
            std::cerr << "Invalid host pointer.";
            break;
        case CL_INVALID_MEM_OBJECT:
            std::cerr << "Invalid memory object.";
            break;
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
            std::cerr << "Invalid image format descriptor.";
            break;
        case CL_INVALID_IMAGE_SIZE:
            std::cerr << "Invalid image size.";
            break;
        case CL_INVALID_SAMPLER:
            std::cerr << "Invalid sampler.";
            break;
        case CL_INVALID_BINARY:
            std::cerr << "Invalid binary.";
            break;
        case CL_INVALID_BUILD_OPTIONS:
            std::cerr << "Invalid build options.";
            break;
        case CL_INVALID_PROGRAM:
            std::cerr << "Invalid program.";
            break;
        case CL_INVALID_PROGRAM_EXECUTABLE:
            std::cerr << "Invalid program executable.";
            break;
        case CL_INVALID_KERNEL_NAME:
            std::cerr << "Invalid kernel name.";
            break;
        case CL_INVALID_KERNEL_DEFINITION:
            std::cerr << "Invalid kernel definition.";
            break;
        case CL_INVALID_KERNEL:
            std::cerr << "Invalid kernel.";
            break;
        case CL_INVALID_ARG_INDEX:
            std::cerr << "Invalid argument index.";
            break;
        case CL_INVALID_ARG_VALUE:
            std::cerr << "Invalid argument value.";
            break;
        case CL_INVALID_ARG_SIZE:
            std::cerr << "Invalid argument size.";
            break;
        case CL_INVALID_KERNEL_ARGS:
            std::cerr << "Invalid kernel arguments.";
            break;
        case CL_INVALID_WORK_DIMENSION:
            std::cerr << "Invalid work dimension.";
            break;
        case CL_INVALID_WORK_GROUP_SIZE:
            std::cerr << "Invalid work group size.";
            break;
        case CL_INVALID_WORK_ITEM_SIZE:
            std::cerr << "Invalid work item size.";
            break;
        case CL_INVALID_GLOBAL_OFFSET:
            std::cerr << "Invalid global offset.";
            break;
        case CL_INVALID_EVENT_WAIT_LIST:
            std::cerr << "Invalid event wait list.";
            break;
        case CL_INVALID_EVENT:
            std::cerr << "Invalid event.";
            break;
        case CL_INVALID_OPERATION:
            std::cerr << "Invalid operation.";
            break;
        case CL_INVALID_GL_OBJECT:
            std::cerr << "Invalid GL object.";
            break;
        case CL_INVALID_BUFFER_SIZE:
            std::cerr << "Invalid buffer size.";
            break;
        case CL_INVALID_MIP_LEVEL:
            std::cerr << "Invalid mip level.";
            break;
        case CL_INVALID_GLOBAL_WORK_SIZE:
            std::cerr << "Invalid globabl work size.";
            break;
        default:
            std::cerr << "Unrecognized error: " << err;
            break;
    }

    std::cerr << std::endl;
}
