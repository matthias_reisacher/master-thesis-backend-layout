/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef GM3_LAYOUT_H
#define GM3_LAYOUT_H

#include "IntGraph.h"
#include "cvec.h"

#include <vector>
#include <string>


namespace GM3 {

    class Layout {
    public:
        static const char delim = '\t';

        //general
        static float initialT; //initial temperature for all levels
        static int coarsestSteps; //steps at the coarsest level of the graph
        static int finestSteps; //steps at the finest (original) level
        static float lambda; //temperature reduction
        static float eta; //softening (eliminates divide-by-0)
        static float repConst;
        static float desLength; //desired spring length
        static bool incremental;//if graph is increment change
        static int graphlevel; //the height of the graph to be calculated

        //multi-level
        static unsigned int coarseningThresh; //maximum top-level graph size
        static float desLengthDecay;
        static float initialTDecay;

        static bool aging;

        //multi-pole
        static float theta; //threshold angle for approximation
        static float err; //percent allowed relative error in approximation
        static float omega; //r_c/r threshold for approximation
        // DON'T SET MANUALLY


#ifdef GM3_LOAD_FROM_FILE
        Layout(const char *fn=0);
#endif

        Layout();

        virtual ~Layout();

#ifdef GM3_LOAD_FROM_FILE
        void loadFile(const char *fn=0);
#endif

        virtual void loadGraph(IntGraph *);

        //accessors
        inline IntGraph *g() { return !graphList.empty() ? &(*currGraph) : NULL; }

        inline IntGraph *graph(int level = 0) { return !graphList.empty() ? &(graphList[level]) : NULL; }

        inline unsigned int height() { return graphList.size(); }

        void boundingBox(float &x0, float &y0, float &x1, float &y1);

        void randomize();

        void randomizeAll();

        // KD tree building operates on currGraph
        void buildKDTree();

        void buildKDTree(unsigned int graphLvl);

        void buildKDTree(unsigned int graphLvl, unsigned int thresh);

    protected:
        std::vector <IntGraph> graphList;
        std::vector<IntGraph>::iterator currGraph;

        //These values are kept for doing user-controlled layout
        unsigned int currStep;
        float currT; //temperature

#ifdef TIME_ACC
        double tdiff(timespec, timespec);
#endif

        //graph initialization
        void buildReductions();
    };

}

#endif
