#ifndef GM3GRAPH_H
#define GM3GRAPH_H

#include "cvec.h"
#include <vector>

namespace GM3 {
    class IntGraph;

    class GPULayout;

// GM3::edge
// id1 and id2 refer to the IDs of the nodes at either end of the
    struct edge {
        unsigned int id1, id2;
        unsigned int age;
        float weight;
        bool newedge;
        bool rendered;

        edge(unsigned int id1 = 0, unsigned int id2 = 0, float weight = 1.f);
    };


// GM3::node
    struct node {
        cvec p, scaledpos;
        float weight, changeweight, energy;
        unsigned int id, internalId;
        unsigned int age;
        bool newnode, changenode;
        bool rendered;
        bool laid;
        int parent; //for internal use
        int degree;
        std::vector<edge *> edges;

        node(cvec p = cvec(), float weight = 1.f, unsigned int id = 0, int parent = -1);
    };


// GM3::Graph
// Subclass this, and use it to pass a GM3::Graph to the layout method. Nodes
// have both a unique ID, as well as a unique index, where max(index) = |N|-1
    class Graph {
    public:
        Graph() { layout = 0; }

        virtual unsigned int numNodes() = 0;

        virtual unsigned int numEdges() = 0;

        virtual node *getNodeByID(unsigned int) = 0;

        virtual node *getNodeByInd(unsigned int) = 0;

        virtual edge *getEdge(unsigned int) = 0;

        virtual void setNodeByID(unsigned int, node *) = 0;

        virtual void setNodeByInd(unsigned int, node *) = 0;

        virtual void setEdge(unsigned int, edge *) = 0;

        GM3::IntGraph *getGraph(int level);

        GM3::GPULayout *layout;
        float lowerx, upperx, lowery, uppery;
    };

}
#endif // GM3GRAPH_H
