/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#include "IntGraph.h"

#include "KDTree.h"
#include"GPULayout.h"

#include <cmath>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <queue>


#include "timelogger.h"

using namespace std;
using namespace GM3;

#define TIME_TRIALS

bool compareEdge(edge e1, edge e2);

//#define TIME_ACC

#ifdef GM3_LOAD_FROM_FILE
IntGraph::IntGraph(const char *fn):
    nodes(), edges(), kd(NULL)
{
  changeimpact = 0;
    loadFromFile(fn);
}
#endif

IntGraph::IntGraph() :
        nodes(), edges(), kd(NULL) { changeimpact = 0; }

IntGraph::IntGraph(vector <node> n, vector <edge> e) :
        nodes(), edges(), kd(NULL) {
    changeimpact = 0;
    loadFromPassed(n, e);
}

IntGraph::~IntGraph() {
    if (kd) delete kd;
}


void IntGraph::getNodePositions(float *b) {
    for (unsigned int i = 0; i < numNodes(); i++) {
        b[i * 2 + 0] = nodes[i].p.x();
        b[i * 2 + 1] = nodes[i].p.y();
    }
}

void IntGraph::setNodePositions(float *b) {
    for (unsigned int i = 0; i < numNodes(); i++) {
        nodes[i].p.x(b[i * 2 + 0]);
        nodes[i].p.y(b[i * 2 + 1]);
    }
}

void IntGraph::getNodeWeights(float *b) {
    for (unsigned int i = 0; i < numNodes(); i++) b[i] = nodes[i].weight;
}

void IntGraph::getEdgeWeights(float *b) {
    for (unsigned int i = 0; i < numEdges(); i++) b[i] = edges[i].weight;
}


void IntGraph::loadFromPassed(vector <node> &n, vector <edge> &e) {
    //load the nodes & edges
    nodes = n;
    edges = e;

    formatLoadedData();
}

#ifdef GM3_LOAD_FROM_FILE
void IntGraph::loadFromFile(const char *filename){
    if(!filename) return;

    string fn(filename);
    string ext = fn.substr(fn.find_last_of('.')+1);
    int ret = 0;

    if(ext.compare("nod") == 0) ret = loadNodEdg(fn);
    else if(ext.compare("mtx") == 0) ret = loadMTX(fn);

    if(ret) formatLoadedData();
}

int IntGraph::loadNodEdg(string nfn){
    string efn = nfn.substr(0, nfn.find_last_of('.')).append(".edg");
    return loadNodeFile(nfn.c_str()) && loadEdgeFile(efn.c_str());
}

int IntGraph::loadNodeFile(string fn){
    stringstream l;
    char buf[buffsize];

    int id = 0;
    string name;
    ifstream nodefile(fn.c_str());
    if(!nodefile.is_open()){ //check file open success
        cerr << "Could not open node file \"" << fn << "\"" << endl;
        return 0;
    }

    nodefile.getline(buf, buffsize);
    while(!nodefile.eof()){
        l << buf;
        l >> id >> name;
        nodes.push_back(node(cvec(), name));
        l.str(""); l.clear();
        nodefile.getline(buf, buffsize);
    }
    nodefile.close();

    return 1;
}

int IntGraph::loadEdgeFile(string fn){
    stringstream l;
    char buf[buffsize];

    vector<bool> edgeExists(nodes.size()*nodes.size(), false);

    int id1 = 0, id2 = 0;
    float weight = 0.0;
    ifstream edgefile(fn.c_str());
    if(!edgefile.is_open()){ //check file open success
        cerr << "Could not open edge file \"" << fn << "\"" << endl;
        return 0;
    }

    edgefile.getline(buf, buffsize);
    while(!edgefile.eof()){
        l << buf;
        l >> id1 >> id2 >> weight;
        if(id1 != id2){
            edges.push_back(edge(id1-1, id2-1, weight));
            edgeExists[(id1-1)*numNodes() + (id2-1)] = true;
        }
        l.str(""); l.clear();
        edgefile.getline(buf, buffsize);
    }
    edgefile.close();

    return 1;
}

void IntGraph::skip(ifstream &in, char c){
    while(in.good() && in.peek() == c) in.ignore();
}

int IntGraph::processMTXHeader(ifstream &f){
    string object, format, qualifier;
    f.ignore(buffsize, ' '); //skip %%MatrixMarket
    int vals = 0;

    //grab the object
    skip(f, ' ');
    object.resize(7); //size of longest acceptable object - "matrix"
    getline(f, object, ' ');
    if(object.compare("matrix") != 0){
        cout << "Refusing to load file: object type is not 'matrix'" << endl;
        return 0;
    }

    //grab the format
    skip(f, ' ');
    format.resize(11); //size of longest acceptable format - "coordinate"
    getline(f, format, ' ');
    if(format.compare("coordinate") != 0){
        cout << "Refusing to load file: format type is not 'coordinate'" <<
            endl;
        return 0;
    }

    //grab the first qualifier
    skip(f, ' ');
    qualifier.resize(15); //size of longest qualifier - "skew-symmetric"
    getline(f, qualifier, ' ');
    if(qualifier.compare("pattern") == 0) vals = 2;
    else if(qualifier.compare("real") == 0) vals = 3;
    else if(qualifier.compare("integer") == 0) vals = 3;
    else if(qualifier.compare("complex") == 0) vals = 4;

    //leave the pointer on the next line
    f.ignore(buffsize, '\n');

    if(!vals) cout << "Refusing to load file: unrecognized qualifier: " <<
        qualifier << endl;

    return vals;
}

int IntGraph::processMTXComments(ifstream &f){
    while(f.good() && f.peek() == '%') f.ignore(buffsize, '\n'); //skip comments

    return f.good();
}

int IntGraph::processMTXStats(ifstream &f, unsigned int &n, unsigned int &e){
    unsigned int rows, columns, entries;
    rows = columns = entries = 0;

    skip(f, ' ');
    f >> rows;
    skip(f, ' ');
    f >> columns;
    skip(f, ' ');
    f >> entries;

    //leave the pointer on the next line
    f.ignore(buffsize, '\n');

    if(rows == 0 || columns == 0 || entries == 0){
        cout << "Could not load file: matrix statistics indicate empty matrix."
            << endl;
        return 0;
    }

    if(rows != columns){
        cout << "Could not load file: matrix dimensions not square." << endl;
        return 0;
    }

    n = rows;
    e = entries;
    return 1;
}

void IntGraph::createBlankNodes(unsigned int N){
    nodes = vector<node> (N, node());
}

void IntGraph::fileCleanup(ifstream &f){
    f.close();
}

int IntGraph::processMTXPatternLines(ifstream &f, unsigned int N, unsigned int E){
    //create the nodes
    createBlankNodes(N);

    edges.reserve(E);
    for(unsigned int i=0; f.good() && i < E; i++){
        edges.push_back(edge());
        f >> edges[i].id1 >> edges[i].id2;
        edges[i].id1--;
        edges[i].id2--;
        f.ignore(buffsize, '\n');
    }

    return 1;
}

int IntGraph::processMTXValuedLines(ifstream &f, unsigned int N, unsigned int E){
    //create the nodes
    createBlankNodes(N);

    edges.reserve(E);
    for(unsigned int i=0; f.good() && i < E; i++){
        edges.push_back(edge());
        //read in and ignore the edge value
        f >> edges[i].id1 >> edges[i].id2;
        edges[i].id1--;
        edges[i].id2--;
        f.ignore(buffsize, '\n');
    }

    return 1;
}

int IntGraph::loadMTX(string fn){
    ifstream f(fn.c_str());
    if(!f.is_open()){ //check file open success
        cerr << "Could not open file \"" << fn << "\"" << endl;
        return 0;
    }

    unsigned int entL; //entries per line - 2 if integer, 1 if pattern
    unsigned int N, E; //number of nodes, edges

    if( !(entL = processMTXHeader(f)) ){ fileCleanup(f); return 0; }
    if( !processMTXComments(f)){ fileCleanup(f); return 0; }
    if( !processMTXStats(f, N, E)){ fileCleanup(f); return 0; }

    int ret = 0;
    if(entL == 2) ret = processMTXPatternLines(f, N, E);
    else ret = processMTXValuedLines(f, N, E);

    fileCleanup(f);
    return ret;
}
#endif

void IntGraph::formatLoadedData() {
#ifdef TIME_ACC
    TimeLogger::Instance()->push("Formatting the data.");
    TimeLogger::Instance()->start();
#endif

    std::vector<int> edgecount;
    edgecount.resize(nodes.size(), 0);

    for (unsigned int i = 0; i < nodes.size(); i++)
        IDToInd[nodes[i].id] = i;


    //translate node IDs in edges to indices
    for (unsigned int i = 0; i < edges.size(); i++) {
        edgecount[IDToInd[edges[i].id1]] += 1;
        edgecount[IDToInd[edges[i].id2]] += 1;
        nodes[IDToInd[edges[i].id1]].degree += 1;
        nodes[IDToInd[edges[i].id2]].degree += 1;


        edges[i].id1 = IDToInd[edges[i].id1];
        edges[i].id2 = IDToInd[edges[i].id2];

    }

#ifdef TIME_ACC
    TimeLogger::Instance()->markIt("Resampling: ");
#endif


    vector<bool> visited;
    visited.resize(nodes.size(), false);

    queue < node * > myqueue;

    int count = 0;
    float x, y;

    float mylowerx, myupperx, mylowery, myuppery;

    boundingBox(mylowerx, mylowery, myupperx, myuppery);
#ifndef TIME_TRIALS
    printf("%f %f %f %f \n", mylowerx, myupperx, mylowery, myuppery);
#endif

    //edges have node index
    //nodes->edges have id
    for (unsigned int i = 0; i < nodes.size(); i++) {
        if (visited[i])
            continue;
        myqueue.push(&nodes[i]);
        while (!myqueue.empty()) {
            node *node = myqueue.front();
            myqueue.pop();
            visited[IDToInd[node->id]] = true;
            x = 0;
            y = 0;
            count = 0;
            //add nodes to the queue
            for (unsigned int j = 0; j < node->edges.size(); j++) {
                GM3::edge *edge = node->edges[j];
                if (!visited[IDToInd[edge->id2]]) {
                    myqueue.push(&nodes[IDToInd[edge->id2]]);
                    visited[IDToInd[edge->id2]] = true;
                }
            }//for each edge

            if (node->newnode) {
                for (unsigned int j = 0; j < node->edges.size(); j++) {
                    GM3::edge *edge = node->edges[j];
                    if (!nodes[IDToInd[edge->id2]].newnode || visited[IDToInd[edge->id2]]) {
                        x += nodes[IDToInd[edge->id2]].p.x();
                        y += nodes[IDToInd[edge->id2]].p.y();
                        count++;
                    }//if
                }//for each edge

                if (count == 0) {
                    node->p.x(rand() * 0.8 / RAND_MAX * (myupperx - mylowerx) + mylowerx + 0.1 * (myupperx - mylowerx));
                    node->p.y(rand() * 0.8 / RAND_MAX * (myuppery - mylowery) + mylowery + 0.1 * (myuppery - mylowery));
                } else if (count == 1) {
                    float random = rand() * 1.0 / RAND_MAX * 2 * M_PI;
                    node->p.x(x + cos(random) * Layout::desLength);
                    node->p.y(y + sin(random) * Layout::desLength);
                } else {
                    node->p.x(x / count);
                    node->p.y(y / count);
                }
            }
        }//while stack has item
    }//for nodes

    node *node1 = 0, *node2 = 0;
    //For edges
    for (unsigned int i = 0; i < edges.size(); i++) {
        edge *edge = &edges[i];
        if (edge->newedge) {

            if (nodes[edge->id1].edges.size() > 1 && nodes[edge->id2].edges.size() > 1) {
                nodes[edge->id1].newnode = true;
                nodes[edge->id2].newnode = true;
                continue;
            }

            node1 = 0;
            node2 = 0;

            if (nodes[edge->id1].edges.size() == 1) {
                node1 = &nodes[edge->id2];
                node2 = &nodes[edge->id1];
                nodes[edge->id1].newnode = true;
            } else if (nodes[edge->id2].edges.size() == 1) {
                node1 = &nodes[edge->id1];
                node2 = &nodes[edge->id2];
                nodes[edge->id2].newnode = true;
            }
            float random = rand() / RAND_MAX * 2 * M_PI;
            node2->p.x(node1->p.x() + cos(random) * Layout::desLength);
            node2->p.y(node1->p.y() + sin(random) * Layout::desLength);
        }//new edge
    }//for each edge

    for (unsigned int i = 0; i < nodes.size(); i++) {
        if (nodes[i].changenode) {
            nodes[i].newnode = true;
            nodes[i].changeweight = 1;
        }
    }

    if (layout->aging) {
        networkAging();
    }

#ifdef TIME_ACC
    TimeLogger::Instance()->markIt("Marking new nodes time: ");
#endif



    //remove duplicate edges, and create a temp edge matrix
/*
    vector<bool> edgeExists(nodes.size()*nodes.size(), false);
    for(vector<edge>::iterator i=edges.begin(); i<edges.end(); i++){
        if( (*i).id1 == (*i).id2 ) edges.erase(i);
        else edgeExists[((*i).id1)*numNodes() + ((*i).id2)] = true;
    }
*/
#ifdef TIME_ACC
    TimeLogger::Instance()->markIt("edge matrix: ");
#endif

/*
    //make sure all reverse edges exist
    int initialEdges = numEdges();
    for(int i=0; i<initialEdges; i++){
//        if(!edgeExists.at(edges[i].id2*numNodes() + edges[i].id1))
            edges.push_back(edge(edges[i].id2, edges[i].id1, edges[i].weight));
    }
*/
#ifdef TIME_ACC
    TimeLogger::Instance()->markIt("building reverse edges: ");
#endif
    if (!layout->incremental)
        randomize();

#ifdef TIME_ACC
    TimeLogger::Instance()->markIt("randomizing position: ");
    TimeLogger::Instance()->pop("Formatting the data took: ");
#endif

}

void IntGraph::networkAging() {
    // update ages for existing nodes; new nodes/edges already at age 1
    for (unsigned int i = 0; i < nodes.size(); i++) {
        if (nodes[i].newnode) continue;

        if (nodes[i].edges.size() == 0) {
            nodes[i].age += 1;
        } else {
            unsigned int total, remained, addition, deletion;
            total = remained = addition = deletion = 0;
            for (unsigned int e = 0; e < nodes[i].edges.size(); e++) {
                GM3::edge *edge = nodes[i].edges[e];
                if (edge->newedge || nodes[IDToInd[edge->id2]].newnode) {
                    addition++;
                }
            }
            // TODO: deletion case
            remained = nodes[i].edges.size();
            total = remained + addition + deletion;
            nodes[i].age = nodes[i].age * (remained / total) + 1;

        }

        // Apply age decay for all existing nodes
        float agingRate = 0.5; // age assumption
        nodes[i].changeweight = exp(-agingRate * nodes[i].age);

    }

    for (unsigned int i = 0; i < edges.size(); i++) {
        if (edges[i].newedge) continue;
        edges[i].age += 1;
    }
}

bool compareEdge(edge e1, edge e2) {
    return e1.id1 < e2.id1 || (e1.id1 == e2.id1 && e1.id2 < e2.id2);
}

bool compareNode(node *n1, node *n2) {

    return n1->degree > n2->degree;
}

void IntGraph::sortEdges() {
    sort(edges.begin(), edges.end(), compareEdge);
}

void IntGraph::randomize() {
    //distribute the points randomly
    srand(time(NULL));

    if (nodes.empty()) return;

    for (int i = 0; i < (int) nodes.size(); i++) {
        nodes[i].p.x((float) rand() / (float) RAND_MAX);
        nodes[i].p.y((float) rand() / (float) RAND_MAX);
    }

    rescale(0);

    //reorder the nodes
    //std::vector
}

void IntGraph::boundingBox(float &x0, float &y0, float &x1, float &y1) {
    //find the lower left of the bounding box
    x0 = x1 = nodes[0].p.x();
    y0 = y1 = nodes[0].p.y();
    for (unsigned int i = 1; i < numNodes(); i++) {
        x0 = nodes[i].p.x() < x0 ? nodes[i].p.x() : x0;
        y0 = nodes[i].p.y() < y0 ? nodes[i].p.y() : y0;

        x1 = nodes[i].p.x() > x1 ? nodes[i].p.x() : x1;
        y1 = nodes[i].p.y() > y1 ? nodes[i].p.y() : y1;
    }
}

void IntGraph::rescale(int level) {


    if (!Layout::incremental || level != 0) {
        //find the lower left of the bounding box
        lowerx = nodes[0].p.x();
        lowery = nodes[0].p.y();

        upperx = nodes[0].p.x();
        uppery = nodes[0].p.y();

        for (unsigned int i = 1; i < numNodes(); i++) {
            lowerx = nodes[i].p.x() < lowerx ? nodes[i].p.x() : lowerx;
            lowery = nodes[i].p.y() < lowery ? nodes[i].p.y() : lowery;

            upperx = nodes[i].p.x() > upperx ? nodes[i].p.x() : upperx;
            uppery = nodes[i].p.y() > uppery ? nodes[i].p.y() : uppery;
        }

    }
    //scale the graph to the [0,1] box
    for (unsigned int i = 0; i < numNodes(); i++) nodes[i].scaledpos.x((nodes[i].p.x() - lowerx) / (upperx - lowerx));
    for (unsigned int i = 0; i < numNodes(); i++) nodes[i].scaledpos.y((nodes[i].p.y() - lowery) / (uppery - lowery));
}

//Assume that edges are sorted
int IntGraph::findFirstInstance(int id) {
    if (edges.empty()) return 0u;

    int max = edges.size() - 1u, min = 0;
    while (max >= min) {
        int mid = (max - min) / 2 + min;
        if (edges[mid].id1 == id)
            return mid;
        else if (edges[mid].id1 < id)
            min = mid + 1;
        else
            max = mid - 1;
    }

    return edges.size();
}


IntGraph *IntGraph::maxIndptSet() {
    using namespace std;

    sortEdges();

    IntGraph *par = new IntGraph();
    bool visited[numNodes()];
    memset(visited, false, sizeof(bool) * numNodes());
    unsigned int ind = 0;
    unsigned int s = 0;
    unsigned int t = 0;

    vector < GM3::node * > nodebydegree;
    for (unsigned int i = 0; i < nodes.size(); i++) {
        nodes[i].internalId = i;
        nodebydegree.push_back(&nodes[i]);
    }

    sort(nodebydegree.begin(), nodebydegree.end(), compareNode);


    //Pick Nodes based on degree
    //choose the nodes for the parent graph
    while (ind < nodebydegree.size()) {
        if (visited[nodebydegree[ind]->internalId]) {
            ind++;
            continue;
        }

        //add this node
        visited[nodebydegree[ind]->internalId] = true;

        par->addNode(*(nodebydegree[ind]));
        nodebydegree[ind]->parent = par->numNodes() - 1;


        par->nodes[nodebydegree[ind]->parent].scaledpos.x(nodebydegree[ind]->p.x());
        par->nodes[nodebydegree[ind]->parent].scaledpos.y(nodebydegree[ind]->p.y());

        int start = findFirstInstance(nodebydegree[ind]->internalId);

        //remove all adjacent nodes from consideration for the `parent graph
        while (start < edges.size() && edges[start].id1 == nodebydegree[ind]->internalId) {
            unsigned int rind = edges[start].id2;

            if (!visited[rind]) {
                nodes[rind].parent = nodebydegree[ind]->parent;
                par->nodes[nodes[rind].parent].weight += nodes[rind].weight;
                visited[rind] = true;
                if (nodes[rind].newnode) {
                    par->nodes[nodes[rind].parent].newnode = true;
                    par->nodes[nodes[rind].parent].changeweight += nodes[rind].changeweight;
                }
            }
            start++;
        }
        ind++;
    }

/*

    //picks nodes from index left to right
    //choose the nodes for the parent graph
    while(ind < nodes.size()){
        if(visited[ind]){
            ind++;
            continue;
        }

        //add this node
        visited[ind] = true;
        par->addNode(nodes[ind]);
        nodes[ind].parent = par->numNodes()-1;





        par->nodes[nodes[ind].parent].scaledpos.x(nodes[ind].p.x());
        par->nodes[nodes[ind].parent].scaledpos.y(nodes[ind].p.y());

        //determine adjacency list bounds
        s = t;
        while(s < edges.size() && edges[s].id1 < ind) s++;
        t = s;
        while(t < edges.size() && edges[t].id1 == ind) t++;


        //remove all adjacent nodes from consideration for the parent graph
        for(unsigned int i=s; i<t; i++){ 
            unsigned int rind = edges[i].id2;
            if(!visited[rind]){
                nodes[rind].parent = nodes[ind].parent;
                par->nodes[nodes[rind].parent].weight += nodes[rind].weight;
                visited[rind] = true;
                if(nodes[rind].newnode){
                  par->nodes[nodes[rind].parent].newnode = true;
                  par->nodes[nodes[rind].parent].changeweight += nodes[rind].changeweight;
                }
            }
        }

        ind++;
    }
*/

    //For each edge, add it to the MIS if the nodes' parents differ.
    for (unsigned int i = 0; i < edges.size(); i++) {
        //  printf("Edge:%d id1:%d id2:%d\n", i, edges[i].id1, edges[i].id2);
        unsigned int p1 = nodes[edges[i].id1].parent;
        unsigned int p2 = nodes[edges[i].id2].parent;
        float w = edges[i].weight;
        if (p1 != p2) par->addEdge(edge(p1, p2, w));
    }

    //sort
    par->sortEdges();

    //mark the duplicate edges
    vector<bool> edgeAccept(par->edges.size(), true);
    for (unsigned int i = 1; i < par->edges.size(); i++) {
        if (edgeEqual(par->edges[i - 1], par->edges[i])) {
            edgeAccept[i - 1] = false;
            par->edges[i].weight += par->edges[i - 1].weight;
        }
    }

    unsigned int sz = 0;
    //find first unaccepted edge
    while (sz < edgeAccept.size() && edgeAccept[sz]) sz++;

    //condense the list
    for (unsigned int i = sz + 1; i < par->edges.size(); i++) {
        if (edgeAccept[i]) par->edges[sz++] = par->edges[i];
    }

    par->edges.resize(sz);

    changeimpact = 0;
    //finds how much the biggest node will move
    for (unsigned int i = 0; i < nodes.size(); ++i) {
        float percent = nodes[i].changeweight / nodes[i].weight;

        //printf("Change:%f Weight:%f Percent:%f\n", nodes[i].changeweight,nodes[i].weight, percent);
        if (changeimpact < percent) {
            changeimpact = percent;
        }
    }
    return par;
}

//TODO:get rid of the jitter and use previous location
void IntGraph::setLayoutFromMIS(IntGraph *g, float jitter) {
    for (unsigned int i = 0; i < numNodes(); i++) {

        nodes[i].p = g->nodeAt(nodes[i].parent).p;
    }

    srand(time(NULL));
    for (int i = 0; i < (int) nodes.size(); i++) {
        nodes[i].p.x(nodes[i].p.x() + jitter * (float) rand() / (float) RAND_MAX);
        nodes[i].p.y(nodes[i].p.y() + jitter * (float) rand() / (float) RAND_MAX);
    }
}

void IntGraph::setLayoutFromMISOffset(IntGraph *g) {
    float diffx, diffy;

    for (unsigned int i = 0; i < numNodes(); i++) {
        //printf("Node id:%d x:%f y:%f, Parent x:%f y:%f\n",i, nodes[i].p.x(),nodes[i].p.y(),g->nodeAt(nodes[i].parent).p.x(),g->nodeAt(nodes[i].parent).p.y() );
        //printf("ScaledNode id:%d x:%f y:%f, Parent x:%f y:%f\n",i, nodes[i].scaledpos.x(),nodes[i].scaledpos.y(),g->nodeAt(nodes[i].parent).scaledpos.x(),g->nodeAt(nodes[i].parent).scaledpos.y() );


        diffx = g->nodeAt(nodes[i].parent).p.x() - g->nodeAt(nodes[i].parent).scaledpos.x();
        diffy = g->nodeAt(nodes[i].parent).p.y() - g->nodeAt(nodes[i].parent).scaledpos.y();

        nodes[i].p.x(nodes[i].p.x() + diffx);
        nodes[i].p.y(nodes[i].p.y() + diffy);
    }


}

std::vector<unsigned int> IntGraph::buildKDTree() {
    if (!kd) kd = new KDTree(this);
    return kd->buildTree();
}

std::vector<unsigned int> IntGraph::buildKDTree(unsigned int thresh) {
    if (!kd) kd = new KDTree(this);
    return kd->buildTree(thresh);
}


void IntGraph::translateEdges(std::vector<unsigned int> &trans) {
    for (unsigned int i = 0; i < edges.size(); i++) {
        edges[i].id1 = trans.at(edges[i].id1);
        edges[i].id2 = trans.at(edges[i].id2);
    }
}

void IntGraph::translateParents(std::vector<unsigned int> &trans) {
    for (unsigned int i = 0; i < nodes.size(); i++) {
        if (nodes[i].parent >= 0 && nodes[i].parent < (int) (trans.size()))
            nodes[i].parent = trans.at(nodes[i].parent);
    }
}
