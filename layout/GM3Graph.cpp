#include "GM3Graph.h"

#include "GPULayout.h"

namespace GM3 {
    IntGraph *Graph::getGraph(int level) {
        if (layout)
            return layout->graph(level);
        else
            return NULL;
    }

    node::node(cvec _p, float _w, unsigned int _ID, int _pa) :
            p(_p), weight(_w), id(_ID), parent(_pa) {
        newnode = true;
        rendered = false;
        laid = false;
        changeweight = 1;
        degree = 0;
        age = 1;
        changenode = false;
    }

    edge::edge(unsigned int _1, unsigned int _2, float _w) :
            id1(_1), id2(_2), weight(_w) {
        newedge = true;
        rendered = false;
        age = 1;
    }

}
