/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#include "KDTree.h"

#include <algorithm>

using namespace GM3;

unsigned int KDTree::thresh = 0;

KDTree::KDTree() :
        G(NULL), root(NULL),
        treeSize(0), treeHeight(0), numLeaves(0), avgLeafSize(0.f), maxLeafSize(0) {}

KDTree::KDTree(IntGraph *g) :
        G(g), root(NULL),
        treeSize(0), treeHeight(0), numLeaves(0), avgLeafSize(0.f), maxLeafSize(0) {}

KDTree::~KDTree() {
    freeKDTree(root);
}

void KDTree::freeKDTree(KDTree::kdnode *root) {
    if (!root) return;

    treeSize = 0;
    treeHeight = 0;
    numLeaves = 0;
    avgLeafSize = 0.f;
    maxLeafSize = 0;

    freeKDTree(root->l);
    freeKDTree(root->r);
    delete root;
}

std::vector<unsigned int> KDTree::buildTree() {
    return buildTree(thresh);
}

std::vector<unsigned int> KDTree::buildTree(unsigned int thresh) {
    using namespace std;
    if (!G) return vector<unsigned int>();
    vector <node> nodesCopy(G->nodes);

    //misdirection vector to perform the rearranging operations on without
    //losing node IDs
    vector<unsigned int> misd(G->nodes.size());
    for (unsigned int i = 0; i < misd.size(); i++) misd[i] = i;

    //clear out any old KD trees
    freeKDTree(root);

    //make a KD tree by calling the recursive function on the root
    root = buildSubTree(misd.begin(), misd.begin(), misd.end(), 0, thresh,
                        boundingBox());

    //create a copy of the nodes vector to move them all in one step
    for (unsigned int i = 0; i < G->nodes.size(); i++)
        G->nodes[i] = nodesCopy.at(misd[i]);

    //translation vector for rearranging the nodes and changing their IDs
    vector<unsigned int> trans(misd.size());
    for (unsigned int i = 0; i < misd.size(); i++) trans.at(misd[i]) = i;


    //calculate average leaf size
    avgLeafSize = (float) G->numNodes() / (float) numLeaves;

    //translate the graph to the new node order
    G->translateEdges(trans);
    //G->translateParents(trans);

    return trans;
}


KDTree::kdnode *KDTree::buildSubTree(std::vector<unsigned int>::iterator first,
                                     std::vector<unsigned int>::iterator s,
                                     std::vector<unsigned int>::iterator t,
                                     unsigned int d, unsigned int thresh, KDTree::boundingBox bb) {
    using namespace std;
    unsigned int sz = (unsigned int) (t - s);

    if (sz <= 0) return NULL;

    kdnode *n = new kdnode(s - first, t - first);
    n->bb = bb;

    //statistics
    treeSize += 1;
    treeHeight = d + 1 > treeHeight ? d + 1 : treeHeight;


    if (sz <= thresh) { //this is a leaf node
        numLeaves++;
        maxLeafSize = sz > maxLeafSize ? sz : maxLeafSize;
        avgLeafSize += sz; //sum for now, and divide at the end
        n->p = findCenter(s, t);
        n->d = findRadius(s, t, n->p);
        n->w = sumWeight(s, t);
        return n;
    }

    float m;
    boundingBox bb0 = bb;
    boundingBox bb1 = bb;
    vector<unsigned int>::iterator s0 = s;
    vector<unsigned int>::iterator t0s1;
    vector<unsigned int>::iterator t1 = t;
    if (d % 2 == 0) { //split on x
        m = medianX(s, t);
        t0s1 = pivotSortX(s, t, m);
        bb0.ur.x(m);
        bb1.ll.x(m);
    } else { //split on y
        m = medianY(s, t);
        t0s1 = pivotSortY(s, t, m);
        bb0.ur.y(m);
        bb1.ll.y(m);
    }

    if (t0s1 - s0 == 0 || t1 - t0s1 == 0) {
        numLeaves++;
        maxLeafSize = sz > maxLeafSize ? sz : maxLeafSize;
        avgLeafSize += sz; //sum for now, and divide at the end
        n->p = findCenter(s, t);
        n->d = findRadius(s, t, n->p);
        n->w = sumWeight(s, t);
        return n;
    }

    //set up the child trees
    n->m = m;
    n->l = buildSubTree(first, s0, t0s1, d + 1, thresh, bb0);
    n->r = buildSubTree(first, t0s1, t1, d + 1, thresh, bb1);

    //determine center and radius from left and right child
    n->p = (n->l->p + n->r->p) / 2.f;
    float dl = n->l->p.mag() + n->l->d;
    float dr = n->r->p.mag() + n->r->d;
    n->d = dr > dl ? dr : dl;

    //sum the weight of the two children
    n->w = n->l->w + n->r->w;

    return n;
}

cvec KDTree::findCenter(std::vector<unsigned int>::iterator s,
                        std::vector<unsigned int>::iterator t) {
    using namespace std;

    //just average all positions
    cvec avg = cvec();
    for (vector<unsigned int>::iterator i = s; i < t; i++) {
        avg = avg + G->nodes[*i].p;
    }

    return avg / (float) (t - s);
}

float KDTree::findRadius(std::vector<unsigned int>::iterator s,
                         std::vector<unsigned int>::iterator t, cvec c) {
    using namespace std;

    float d = 0.f;
    float r = 0.f;
    for (vector<unsigned int>::iterator i = s; i < t; i++) {
        d = (G->nodes[*i].p - c).mag();
        r = d > r ? d : r;
    }

    return r;
}

float KDTree::sumWeight(std::vector<unsigned int>::iterator s,
                        std::vector<unsigned int>::iterator t) {
    using namespace std;

    float w = 0.f;
    for (vector<unsigned int>::iterator i = s; i < t; i++) {
        w += G->nodes[*i].weight;
    }

    return w;
}

std::vector<unsigned int>::iterator KDTree::pivotSortX(
        std::vector<unsigned int>::iterator s,
        std::vector<unsigned int>::iterator t, float m) {
    unsigned int swp;
    std::vector<unsigned int>::iterator store = s;
    std::vector<unsigned int>::iterator i;
    for (i = s; i < t; i++) {
        if (G->nodes[*i].p.x() < m) {
            swp = *store;
            *store = *i;
            *i = swp;
            store++;
        }
    }

    return store;
}

std::vector<unsigned int>::iterator KDTree::pivotSortY(
        std::vector<unsigned int>::iterator s,
        std::vector<unsigned int>::iterator t, float m) {
    unsigned int swp;

    std::vector<unsigned int>::iterator store = s;
    std::vector<unsigned int>::iterator i;
    for (i = s; i < t; i++) {
        if (G->nodes[*i].p.y() < m) {
            swp = *store;
            *store = *i;
            *i = swp;
            store++;
        }
    }

    return store;
}


bool compNodeX(node n0, node n1) { return n0.p.x() < n1.p.x(); }

float KDTree::medianX(std::vector<unsigned int>::iterator s,
                      std::vector<unsigned int>::iterator t) {
    if (t - s > 1) {
        float m;
        //get a set of nodes to choose a pivot from
        std::vector <node> set = pivotSet(s, t);

        //sort the set of nodes
        sort(set.begin(), set.end(), compNodeX);

        unsigned int i = set.size() / 2;
        m = (set[i].p.x() + set[i - 1].p.x()) / 2.f;
        if (m != m) { //m is NaN
            std::cout << "I accidentally a whole median." << std::endl;
        }
        return m;
    } else if (t - s > 0) {
        return *s;
    } else {
        return 0.f;
    }
}


bool compNodeY(node n0, node n1) { return n0.p.y() < n1.p.y(); }

float KDTree::medianY(std::vector<unsigned int>::iterator s,
                      std::vector<unsigned int>::iterator t) {
    if (t - s > 1) {
        float m;
        //get a set of nodes to choose a pivot from
        std::vector <node> set = pivotSet(s, t);

        //sort the set of nodes
        sort(set.begin(), set.end(), compNodeY);

        unsigned int i = set.size() / 2;
        m = (set[i].p.y() + set[i - 1].p.y()) / 2.f;
        if (m != m) { //m is NaN
            std::cout << "I accidentally a whole median." << std::endl;
        }
        return m;
    } else if (t - s > 0) {
        return *s;
    } else {
        return 0.f;
    }
}


std::vector <node> KDTree::pivotSet(
        std::vector<unsigned int>::iterator s,
        std::vector<unsigned int>::iterator t) {
    using namespace std;
    unsigned int sz = (unsigned int) (t - s);
    vector<unsigned int> set;
    vector <node> nodeSet;

    //if the desired space is bigger than the given number of nodes,
    //just use the whole set
    if (pivotSpaceSize >= sz) {
        nodeSet.reserve(sz);
        for (vector<unsigned int>::iterator i = s; i < t; i++)
            nodeSet.push_back(G->nodes[*i]);
        return nodeSet;
    }

    //choose a set of unique random nodes to search for a pivot from

    set.reserve(pivotSpaceSize);
    nodeSet.reserve(pivotSpaceSize);
    srand(time(NULL));
    for (unsigned int i = 0; i < pivotSpaceSize; i++) {
        bool unique = false;
        unsigned int r;
        while (!unique) {
            r = (unsigned int) rand() % sz;
            unique = true;
            vector<unsigned int>::iterator j = set.begin();
            while (j < set.end() && unique) { unique = !(*(j++) == r); }
        }
        set.push_back(s[r]);
    }

    for (vector<unsigned int>::iterator i = set.begin(); i < set.end(); i++) {
        nodeSet.push_back(G->nodes[*i]);
    }
    return nodeSet;
}


KDTree::kdnode *KDTree::findLeaf(float x, float y) {
    kdnode *n = root;
    if (!n) return NULL;
    int d = 0;
    while (n->l && n->r) {
        if ((d % 2 == 0 && x < n->m) || (d % 2 == 1 && y < n->m)) n = n->l;
        else if ((d % 2 == 0 && x >= n->m) || (d % 2 == 1 && y >= n->m)) n = n->r;
        d++;
    }

    return n;
}
