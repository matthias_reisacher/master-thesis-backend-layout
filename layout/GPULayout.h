/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef GM3_GPULAYOUT_H
#define GM3_GPULAYOUT_H

#include "Layout.h"
#include "KDTree.h"
#include "oclUtil.h"

namespace GM3 {

    class GPULayout : public Layout {
    public:
        struct __attribute__((aligned(4))) kdnode {
            cl_float x;
            cl_float y;
            cl_float d;
            cl_float w;
            cl_uint s;
            cl_uint t;
            cl_int l;
            cl_int r;
        };

        static unsigned int n;

#ifdef GM3_LOAD_FROM_FILE
        GPULayout(const char *fn=0);
#endif

        GPULayout();

        ~GPULayout();

        void loadGraph(IntGraph *g);

        void computeLayout();

        std::vector<float> &computeGraphTension();

        void computeOneGraphStep(bool fullrelayout, std::map<int, bool> list);

        bool refinement;
    private:
        //OpenCL state variables
        cl_context context;
        cl_device_id *devices;
        cl_command_queue commandQueue;
        cl_program OCLProg, OCLProg2;

        //OCL kernel handles
        cl_kernel stepKern, potentialKern;

        //OpenCL memory arrays
        cl_mem nodes_b;
        cl_mem CSRN_b;
        cl_mem CSRE_b;
        cl_mem nodeW_b;
        cl_mem edgeW_b;
        cl_mem KDNodes_b;
        cl_mem nodemask_b;
        cl_mem nodetemp_b;

        cl_mem pe_results_b;

        //CPU-side data formatted for the GPU - keeping these buffers reduces
        //the number of allocations. Buffers are kept only for frequently 
        //rewritten data.
        std::vector <cl_float> GPUTmp; //size of the larger of |N|*2 and |E|
        std::vector <cl_int> NodeMask;
        std::vector <kdnode> GPUKDNodes;
        std::vector <cl_uint> CSRNodes;
        std::vector <cl_uint> CSREdges;
        std::vector <cl_float> NodeTemp;
        std::vector <cl_float> PE_results;//results of the enegry



        //CPU-side data formatted for the GPU - keeping these buffers reduces
        //the number of allocations. Buffers are kept only for frequently
        //rewritten data.
        //Potential Energy
        std::vector <cl_float> PE_GPUTmp; //size of the larger of |N|*2 and |E|
        std::vector <kdnode> PE_GPUKDNodes;
        std::vector <cl_uint> PE_CSRNodes;
        std::vector <cl_uint> PE_CSREdges;


        //cleanup
        void cleanupCL();

        void cleanupCLMem();


        //CSR format--will be used internally by the GPU
        // - 'Compressed Sparse Row'
        // - The node list holds the starting location of the adj list for each
        //   node. Each adjacency list is bound by the start of the next list.
        // - The edge list is a sequential set of adjacency lists
        void buildCSR();

        //initialization - in order of increasing frequency
        void initOnce();

        void initPerGraph();

        void initPerLayout();

        unsigned int initPerLevel();

        void initPerKDTree();

        void initPerStep();

        bool KDRebuildStep(unsigned int step, unsigned int lastStep);

        void buildKDTree();

        void layoutLevel();

        void layoutStep();


        //Potential Energy
        void computePotentialEnergy();

        //graph conversion
        void convertGraphNodePosToGPU(std::vector <node> &nodes,
                                      std::vector <cl_float> &pos);

        void convertGraphNodePosToCPU(std::vector <cl_float> &pos,
                                      std::vector <node> &nodes);

        void convertGraphNodeWToGPU(std::vector <node> &nodes,
                                    std::vector <cl_float> &weights);

        void convertGraphEdgeWToGPU(std::vector <edge> &edges,
                                    std::vector <cl_float> &weights);


        //multipole
        kdnode convertKDNode(KDTree::kdnode *);

        void convertKDTree(KDTree *, std::vector <kdnode> &);

        void rStacklessConv(std::vector <GPULayout::kdnode> &, int, int);
    };

} //end GM3 namespace

#endif
