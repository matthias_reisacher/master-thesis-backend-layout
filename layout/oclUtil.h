/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// This OpenCL utility header contains functions to simplify OpenCL calls. 
// Rather than calling an error checking function after every ocl function, 
// error checking is toggled by a define at the beginning of the file (on by 
// default). This simplifies the code and gives it a cleaner appearance. 
// 
// TO DO:
//  - Add a way to do a 'cleanup' callback and exit from the code. Right now
//    the error is only reported to cerr; sometimes exit is called.
////////////////////////////////////////////////////////////////////////////////

#ifndef GM3_OCLUTIL_H
#define GM3_OCLUTIL_H

#ifdef __APPLE__
#include <openCL/cl.h>
#else

#include <CL/cl.h>

#endif

#ifndef CLU_NO_DEBUG
//defines to automatically send the file names and line numbers
#define cluGetPlatformIDs(a, b, c) \
        _cluGetPlatformIDs(__FILE__,__LINE__,a,b,c)
#define cluPrintPlatformVersion(a) \
        _cluPrintPlatformVersion(__FILE__,__LINE__,a)
#define cluGetContextInfo(a, b, c, d, e) \
        _cluGetContextInfo(__FILE__,__LINE__,a,b,c,d,e)
#define cluBuildProgram(a, b, c, d, e, f) \
        _cluBuildProgram(__FILE__,__LINE__,a,b,c,d,e,f)
#define cluSetKernelArg(a, b, c, d) \
        _cluSetKernelArg(__FILE__,__LINE__,a,b,c,d)
#define cluReleaseMemObject(a) \
        _cluReleaseMemObject(__FILE__,__LINE__,a)
#define cluEnqueueWriteBuffer(a, b, c, d, e, f, g, h, i) \
        _cluEnqueueWriteBuffer(__FILE__,__LINE__,a,b,c,d,e,f,g,h,i)
#define cluEnqueueReadBuffer(a, b, c, d, e, f, g, h, i) \
        _cluEnqueueReadBuffer(__FILE__,__LINE__,a,b,c,d,e,f,g,h,i)
#define cluEnqueueNDRangeKernel(a, b, c, d, e, f, g, h, i) \
        _cluEnqueueNDRangeKernel(__FILE__,__LINE__,a,b,c,d,e,f,g,h,i)
#define cluWaitForEvents(a, b) \
        _cluWaitForEvents(__FILE__,__LINE__,a,b)
#define cluFinish(a) \
        _cluFinish(__FILE__,__LINE__,a)

#define cluCreateContextFromType(a, b, c, d) \
        _cluCreateContextFromType(__FILE__,__LINE__,a,b,c,d)
#define cluCreateCommandQueue(a, b, c) \
        _cluCreateCommandQueue(__FILE__,__LINE__,a,b,c)
#define cluCreateProgramWithSource(a, b, c, d) \
        _cluCreateProgramWithSource(__FILE__,__LINE__,a,b,c,d)
#define cluCreateKernel(a, b) \
        _cluCreateKernel(__FILE__,__LINE__,a,b)
#define cluCreateBuffer(a, b, c, d) \
        _cluCreateBuffer(__FILE__,__LINE__,a,b,c,d)
#define cluCreateImage2D(a, b, c, d, e, f, g) \
        _cluCreateImage2D(__FILE__,__LINE__,a,b,c,d,e,f,g)

#endif

//error code as return value
void _cluGetPlatformIDs(const char *, int, cl_uint, cl_platform_id *, cl_uint *);

void _cluPrintPlatformVersion(const char *, int, cl_platform_id platid);

void _cluGetContextInfo(const char *, int, cl_context, cl_context_info, size_t,
                        void *, size_t *);

void _cluBuildProgram(const char *, int, cl_program, cl_uint,
                      const cl_device_id *, const char *,
                      void (*clcb)(cl_program, void *), void *);

void _cluSetKernelArg(const char *, int, cl_kernel, cl_uint, size_t,
                      const void *);

void _cluReleaseMemObject(const char *, int, cl_mem);

void _cluEnqueueWriteBuffer(const char *, int, cl_command_queue, cl_mem, cl_bool,
                            size_t, size_t, const void *, cl_uint, const cl_event *, cl_event *);

void _cluEnqueueReadBuffer(const char *, int, cl_command_queue, cl_mem, cl_bool,
                           size_t, size_t, void *, cl_uint, const cl_event *, cl_event *);

void _cluEnqueueNDRangeKernel(const char *, int, cl_command_queue, cl_kernel,
                              cl_uint, const size_t *, const size_t *, const size_t *, cl_uint,
                              const cl_event *, cl_event *);

void _cluWaitForEvents(const char *, int, cl_uint, const cl_event *);

void _cluFinish(const char *, int, cl_command_queue);

//pointer to error code as parameter
cl_context _cluCreateContextFromType(const char *, int, cl_context_properties *,
                                     cl_device_type,
                                     void (*clcb)(const char *, const void *, size_t, void *), void *);

cl_command_queue _cluCreateCommandQueue(const char *, int, cl_context,
                                        cl_device_id, cl_command_queue_properties);

cl_program _cluCreateProgramWithSource(const char *, int, cl_context, cl_uint,
                                       const char **, const size_t *);

cl_kernel _cluCreateKernel(const char *, int, cl_program, const char *);

cl_mem _cluCreateBuffer(const char *, int, cl_context, cl_mem_flags,
                        size_t, void *);

cl_mem _cluCreateImage2D(cl_context, cl_mem_flags, const cl_image_format,
                         size_t, size_t, size_t, void *);


//utility
void checkProgBuildLog(cl_program prog, cl_device_id did);

char *loadProg(const char *filename);

void parseErr(cl_int err);

#endif
