/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2014 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef GM3_INTERFACE_H
#define GM3_INTERFACE_H


#include "cvec.h"
#include "IntGraph.h"
#include "GPULayout.h"


/*
 * --- Intro to FM3
 * FM3 is short for Fast, Multi-level, Multi-pole Method. At the core of FM3 is
 * standard force-directed graph-layout: each node applies a radius^2 repulsive
 * force to every other node (as if they were electrons), and each edge acts
 * like a spring to keep the two nodes it connects at some desired distance. The
 * forces on each node are summed, and it is moved some small distance. Some
 * sort of friction-like element is applied so that the whole thing eventually
 * stops moving, yielding the final layout.
 *
 * Force-directed layouts are notoriously expensive to calculate (particularly
 * the repulsive force), so FM3 makes a few additions to improve its speed.
 *
 * The multi-level part of the method combines groups of nodes to create super 
 * nodes, which are then used to build a "coarser" graph that is somehow
 * representative of the original. This step is repeated multiple times to get
 * a hierarchy of coarser representations. A layout for the coarsest graph is 
 * computed first, and that layout is used as the substrate for the finer graph
 * that is underneath it. Eventually, this yields a substrate layout for the 
 * original graph that (hopefullY) positions groups of related nodes relative
 * to one another in a logical fashion.
 *
 * The multi-pole method allows a group of distant nodes to be treated as a 
 * single, large node. This means the all-to-all repulsive force becomes an
 * O(N*logN) instead of N^2 (where N is the number of nodes).
 *
 * The "frictional" force is taken care of simulated annealing, which mimics
 * the physical process by which hot matter in the liquid state cools or anneals
 * into a solid. The graph is given an initial temperature, which determines the
 * total distance that any node can move in a single iteration step. This heat
 * decreases each iteration, so that the nodes are eventually stopped from moving.
 * As the algorithm moves from coarser to finer graph levels, the temperature is
 * reset--but to a lower amount each time, to keeps the nodes close to their
 * super node's position from the coarser graph.
 */

//controls whether the old file loading code is enabled
// -- you probably don't want this
//#define GM3_LOAD_FROM_FILE


class Tension {
public:
    Tension(float energy) {
        cost = 0;
        distforce = energy;
        degree = 0;
    }


    int degree;
    float cost, distforce;
};

namespace GM3 {


/*
 * --- Parameterization
 * See "Intro to FM3", above, for a general description of the force-
 * directed graph layout method.
 */
    struct Parameterization {
        //force constants
        float repConst; //node-node repulsive constant
        float desLength; //desired spring length

        //simulated annealing constants
        float initialT; //initial temperature
        float lambda; //per-iteration temperature decay factor

        //multi-level control
        unsigned int coarseningThresh; //graph size (N) at which to halt coarsening
        unsigned int coarsestSteps; //iteration steps for coarsest graph
        unsigned int finestSteps; //iteration steps for finest (original) graph
        float initialTDecay; //reduces initialT for finer graphs
        float desLengthDecay; //reduces desLength for finer graphs

        //multi-pole control
        float theta; //tangent of threshold angle for approximation
        float err; //acceptable approximation error threshold

        bool aging;

        bool incremental;//If true, do an incremental update

        int graphlevel;

        //Initialization and default values
        Parameterization(
                float repConst = 4.f, //4.f
                float desLength = 10.0f, // 0.055f
                float initialT = 1.5f, // 1.5f
                float lambda = 0.95f, // 0.95
                unsigned int coarseningThresh = 20, // 20
                unsigned int coarsestSteps = 250, // 250
                unsigned int finestSteps = 30, // 30
                float initialTDecay = 0.95f, // 0.95f
                float desLengthDecay = 1.3229, // sqrt(7.f/4.f) 1.3229
                float theta = 1.2f, // 1.2f
                float err = 0.01f,
                bool aging = false,
                bool incremental = false,
                int graphlevel = 0);
    };

//other constants
    extern const float eta; //softening factor to avoid divide by 0
    extern const float omega; //placeholder; actual error computer from err


// computeLayout
// GM3::Graph and GM3::Parameterization go in, laid out GM3::Graph comes out
    void computeLayout(Graph *G, Parameterization *P = 0);

    IntGraph sanitize(GM3::Graph *G, Parameterization *P = 0);

    void computeLayout(Graph *G, IntGraph *I);

    void computeGraphTension(Graph *);

    void computeOneGraphStep(Graph *, bool fullrelayout, std::map<int, bool> list);

    void computeOneRefinementStep(Graph *);

    void convertBack(Graph *);

} //end namespace GM3

#endif
