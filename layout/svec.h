/*******************************************************************************
                          GM3 - GPU implementation of FM3
                            ----------------------------
                          Copyright: (C) 2011 by Nick Leaf
                              Email: njleaf@ucdavis.edu
*******************************************************************************/

/***************************************************************************
 *                                                                         *
 * Copyright: All rights reserved.  May not be used, modified, or copied   *
 * without permission.                                                     *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// Description: A vector for 3-space in spherical coordinates with built-in
// implementations of common operations, including conversion to Cartesian 
// coordinates.
//
// NOTE: the spherical coordinate system used by this class is relevant for
// computer graphics (versus, say, physics). The r coordinate retains the same 
// meaning, the phi coordinate refers to angle on the x-z plane, and the theta
// coordinate refers to angular offset from the y-axis.
///////////////////////////////////////////////////////////////////////////////

#ifndef GM3_SVEC_H
#define GM3_SVEC_H

#include <string>
#include <iostream>

class cvec;

class svec {
public:
    // Constructors //
    svec(float r = 0.f, float theta = 0.f, float phi = 0.f);

    svec(float *arr);

    // Accessors //
    float r() const;

    float theta() const;

    float phi() const;

    float *toArray() const;

    std::string toString() const;

    // Mutators //
    void r(float r);

    void theta(float theta);

    void phi(float phi);

    // Coordinate conversion //
    cvec toCartesian() const;

    // Vector operations //
    float mag() const;

    svec mulByScalar(float f) const;

    svec divByScalar(float f) const;

    svec norm() const;

    // Overloaded operators //
    operator float *();

    float &operator[](int i);

    //scalar multiplication and division
    friend svec operator*(float f, svec v);

    friend svec operator*(svec v, float f);

    svec operator/(float f) const;

    //I/O
    friend std::ostream &operator<<(std::ostream &out, const svec &v);

    friend std::istream &operator>>(std::istream &in, svec &v);

private:
    float v[3];
};

#endif
