/**************************************************************************
  NetZen :  Network Sensitivity Analysis and Visualization
                             -------------------
    copyright            : (C) 2009 by Carlos D. Correa
    email                : correac@cs.ucdavis.edu
***************************************************************************/
#include "propertyBag.h"
#include <assert.h>

PropertyBag::PropertyBag(const char *type, const char *name) : Object(type, name) {
}

PropertyBag::~PropertyBag() {
    //  print("PropertyBag.Destroy\n");

    for (map<string, Property *>::const_iterator it = properties.begin(); it != properties.end(); ++it) {
        delete (it->second);
    }

    properties.clear();
}

vector <string> PropertyBag::getProperties() {
    vector <string> vec;
    //printf("PropertyBag.Destroy\n");
    for (map<string, Property *>::const_iterator it = properties.begin(); it != properties.end(); ++it) {
        vec.push_back((it->first));
    }
    return vec;
}

bool PropertyBag::hasProperty(string name) {
    return (properties.find(name) != properties.end());
}

void PropertyBag::setProperty(string name, Property *p) {
    //print("Adding property %s %s\n", name.c_str(), p->toString().c_str());
    properties[name] = p;
}

void PropertyBag::setProperty(string name, string value) {
    Property *prop = getProperty(name);
    if (prop) {
        prop->parseString(value);
    }
}

Property *PropertyBag::getProperty(string name) {
    if (!hasProperty(name)) return 0;
    return properties[name];
}

void PropertyBag::setPropertyString(string name, string s) {
    properties[name] = new StringProperty(s);
}

void PropertyBag::setPropertyString(string name, const char *s) {
    properties[name] = new StringProperty(s);
}


void PropertyBag::setPropertyFloat(string name, float v) {
    if (!hasProperty(name)) {
        properties[name] = new FloatProperty(v);
    } else {
        ((FloatProperty *) properties[name])->set(v);
    }
    //print("Added property value %s=%f\n", name.c_str(), v);
}

void PropertyBag::setPropertyInt(string name, int v) {
    if (!hasProperty(name)) {
        properties[name] = new IntProperty(v);
    } else {
        ((IntProperty *) properties[name])->set(v);
    }
}

void PropertyBag::setPropertyBool(string name, bool v) {
    if (!hasProperty(name)) {
        properties[name] = new BoolProperty(v);
    } else {
        ((BoolProperty *) properties[name])->set(v);
    }
}

void PropertyBag::setPropertyVecInt(string name, vector<int> v) {
    if (!hasProperty(name)) {
        properties[name] = new VecIntProperty(v);
    } else {
        ((VecIntProperty *) properties[name])->set(v);
    }
}

void PropertyBag::setPropertyVecString(string name, vector <string> v) {
    if (!hasProperty(name)) {
        properties[name] = new VecStringProperty(v);
    } else {
        ((VecStringProperty *) properties[name])->set(v);
    }
}

/*
void PropertyBag::setPropertyVec2(string name, float x, float y) {
  properties[name] = new Vec2Property(x,y);
}

void PropertyBag::setPropertyVec2v(string name, float *v) {
  properties[name] = new Vec2Property(v);
}

void PropertyBag::setPropertyVec3(string name, float x, float y, float z) {
  properties[name] = new Vec3Property(x,y,z);
}

void PropertyBag::setPropertyVec3v(string name, float *v) {
  properties[name] = new Vec3Property(v);
}
*/

float PropertyBag::getPropertyFloat(string name, float defaultValue) {
    if (properties.find(name) == properties.end()) {
        //error("Could not find property %s\n", name.c_str());
        return defaultValue;
    }
    FloatProperty *p = (FloatProperty *) (properties[name]);
    return p->value;
}

int PropertyBag::getPropertyInt(string name, int defaultValue) {
    if (properties.find(name) == properties.end()) {
        return defaultValue;
    }

    IntProperty *p = (IntProperty *) (properties[name]);
    return p->value;
}

bool PropertyBag::getPropertyBool(string name, bool defaultValue) {
    if (properties.find(name) == properties.end()) {
        return defaultValue;
    }
    BoolProperty *p = (BoolProperty *) (properties[name]);
    return p->value;
}

vector<int> &PropertyBag::getPropertyVecInt(string name) {
    if (properties.find(name) == properties.end()) {
        vector<int> daf;
        return daf;
    }

    VecIntProperty *p = (VecIntProperty *) (properties[name]);
    return p->value;
}

vector <string> &PropertyBag::getPropertyVecString(string name) {
    if (properties.find(name) == properties.end()) {
        vector <string> daf;
        return daf;
    }

    VecStringProperty *p = (VecStringProperty *) (properties[name]);
    return p->value;
}

/*
void PropertyBag::getPropertyVec2(string name, float *v) {
  Vec2Property* p = (Vec2Property*) properties[name];
  v[0] = p->vector[0];
  v[1] = p->vector[1];
}

void PropertyBag::getPropertyVec3(string name, float *v) {
  Vec3Property* p = (Vec3Property*) (properties[name]);
  v[0] = p->vector[0];
  v[1] = p->vector[1];
  v[2] = p->vector[2];
}
*/
string PropertyBag::getPropertyString(string name) {
    if (properties.find(name) == properties.end()) {
        return "";
    }
    return ((StringProperty *) properties[name])->value;
}


int PropertyBag::getPropertyType(string name) {
    if (properties.find(name) == properties.end()) {
        return PROPERTY_UNKNOWN;
    }
    return properties[name]->getType();
}
