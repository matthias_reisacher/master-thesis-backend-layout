#include "redis_service.h"
#include <iostream>

RedisService::RedisService(std::string host, int port) {
    this->conn = redis3m::connection::create(host, port);
}

std::string RedisService::readInputFile(std::string id) {
    std::string key = this->createRedisKeyInput(id);
    redis3m::reply r = this->conn->run(redis3m::command("GET") << key);
    return r.str();
}

void RedisService::writeOutputFile(std::string id, std::string file) {
    std::string key = this->createRedisKeyOutput(id);
    this->conn->run(redis3m::command("SET") << key << file);
}

void RedisService::deleteInputFile(std::string id) {
    std::string key = this->createRedisKeyInput(id);
    redis3m::reply r = this->conn->run(redis3m::command("DEL") << key);
}

std::string RedisService::createRedisKeyInput(std::string id) {
    return "layout:" + id + ":raw";
}

std::string RedisService::createRedisKeyOutput(std::string id) {
    return "layout:" + id + ":processed";
}