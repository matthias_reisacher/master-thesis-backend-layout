#!/usr/bin/env bash
cd /home/dago

# Compile protobufs
if [[ -f compile-protos.sh ]]; then
    ./compile-protos.sh
else
    (>&2 echo "Could not compile protos")
    exit 1
fi

# Clean up
[[ -f Makefile ]] && rm Makefile
[[ -f LayoutCode ]] && rm LayoutCode
[[ -d results ]] && rm results/*

# Build and run project
qmake -o Makefile LayoutCode.pro
make
#./LayoutCode example/small/ results/
./LayoutCode